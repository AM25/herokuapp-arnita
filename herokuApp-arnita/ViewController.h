//
//  ViewController.h
//  herokuApp-arnita
//
//  Created by Arnita Martiana on 17/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>{
    UIRefreshControl *mRefreshControl;
    NSMutableArray *arrayTableData;
    NSArray *mDatalist;
    NSArray *contactDetails;
    NSMutableArray *arraySearchContactData;
}
@property (strong, nonatomic) IBOutlet UISearchBar *searchbarContacts;
@property (strong, nonatomic) IBOutlet UITableView *tableViewContactData;


@end

