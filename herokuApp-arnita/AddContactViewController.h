//
//  AddContactViewController.h
//  herokuApp-arnita
//
//  Created by Arnita Martiana on 18/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFloatLabelTextField.h"

@interface AddContactViewController : UIViewController{
    NSMutableArray *data;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgProfil;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *lbFirstName;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *lbLastName;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *lbAge;
- (IBAction)btnSave:(id)sender;

@end
