//
//  ContactDetailsViewController.m
//  herokuApp-arnita
//
//  Created by Arnita Martiana on 18/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import "ContactDetailsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ContactDetailsViewController ()

@end

@implementation ContactDetailsViewController
@synthesize lbFullName;
@synthesize lbLastName;
@synthesize lbFirstName;
@synthesize imgProfil;
@synthesize contactDetails;
@synthesize lbAge;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"contDetails:%@",
          contactDetails);
    lbFirstName.text=[contactDetails valueForKey:@"firstName"];
    lbLastName.text=[contactDetails valueForKey:@"lastName"];
    lbAge.text=[NSString stringWithFormat:@"%@",[contactDetails valueForKey:@"age"]];
    lbFullName.text=[NSString stringWithFormat:@"%@ %@",[[contactDetails valueForKey:@"firstName"]uppercaseString], [[contactDetails valueForKey:@"lastName"]uppercaseString]];
    
    lbFirstName.floatLabelActiveColor      = [UIColor lightGrayColor];
    lbLastName.floatLabelActiveColor      = [UIColor lightGrayColor];
    lbAge.floatLabelActiveColor      = [UIColor lightGrayColor];
    
    imgProfil.layer.cornerRadius=imgProfil.frame.size.height/2;
    imgProfil.layer.masksToBounds = YES;
    imgProfil.layer.borderWidth = 2;
    imgProfil.layer.borderColor=(__bridge CGColorRef _Nullable)([UIColor whiteColor]);
    
    NSString *urlImg =[contactDetails valueForKey:@"photo"];
    [imgProfil sd_setImageWithURL:[NSURL URLWithString:urlImg] placeholderImage:[UIImage imageNamed:@"avatar.png"] options:SDWebImageRefreshCached];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnSave:(id)sender {
    
    NSString *urlString = [NSString stringWithFormat:@"https://simple-contact-crud.herokuapp.com/contact/%@",[contactDetails valueForKey:@"id"]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"PUT"];
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:lbFirstName.text forKey:@"firstName"];
    [dictionary setObject:lbLastName.text forKey:@"lastName"];
    [dictionary setValue:lbAge.text forKey:@"age"];
    [dictionary setValue:@"http://vignette1.wikia.nocookie.net/lotr/images/6/68/Bilbo_baggins.jpg/revision/latest?cb=20130202022550" forKey:@"photo"];
    
    NSLog(@"dictionary:%@", dictionary);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    
    // Append
    [request setHTTPBody:jsonData];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        
        dict = [self cleanJsonToObject:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *message = [dict valueForKey:@"message"];
            
            NSLog(@"response update:%@", dict);
        });
        
    }] resume];
    [self displayMessage];
    
}
-(void)displayMessage{
    UIAlertView *toast = [[UIAlertView alloc]
                          initWithTitle:nil
                          message:@"Contact edited"
                          delegate:nil
                          cancelButtonTitle:nil
                          otherButtonTitles:nil, nil];
    
    toast.backgroundColor=[UIColor redColor];
    [toast show];
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [toast dismissWithClickedButtonIndex:0 animated:YES];
    });
}

- (id)cleanJsonToObject:(id)data
{
    NSError* error;
    if (data == (id)[NSNull null])
    {
        return [[NSObject alloc] init];
    }
    id jsonObject;
    if ([data isKindOfClass:[NSData class]])
    {
        jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    } else
    {
        jsonObject = data;
    }
    if ([jsonObject isKindOfClass:[NSArray class]])
    {
        NSMutableArray *array = [jsonObject mutableCopy];
        for (int i = (int)array.count-1; i >= 0; i--)
        {
            id a = array[i];
            if (a == (id)[NSNull null])
            {
                [array removeObjectAtIndex:i];
            } else
            {
                array[i] = [self cleanJsonToObject:a];
            }
        }
        return array;
    } else if ([jsonObject isKindOfClass:[NSDictionary class]])
    {
        NSMutableDictionary *dictionary = [jsonObject mutableCopy];
        for(NSString *key in [dictionary allKeys])
        {
            id d = dictionary[key];
            if (d == (id)[NSNull null])
            {
                dictionary[key] = @"";
            } else
            {
                dictionary[key] = [self cleanJsonToObject:d];
            }
        }
        return dictionary;
    } else
    {
        return jsonObject;
    }
}
@end
