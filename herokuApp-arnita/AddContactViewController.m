//
//  AddContactViewController.m
//  herokuApp-arnita
//
//  Created by Arnita Martiana on 18/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import "AddContactViewController.h"

@interface AddContactViewController ()

@end

@implementation AddContactViewController
@synthesize lbLastName;
@synthesize lbFirstName;
@synthesize lbAge;
@synthesize imgProfil;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    data= [[NSMutableArray alloc]init];
   
    lbFirstName.floatLabelActiveColor      = [UIColor lightGrayColor];
    lbLastName.floatLabelActiveColor      = [UIColor lightGrayColor];
    lbAge.floatLabelActiveColor = [UIColor lightGrayColor];
    
    imgProfil.layer.cornerRadius=imgProfil.frame.size.height/2;
    imgProfil.layer.masksToBounds = YES;
    imgProfil.layer.borderWidth = 2;
    imgProfil.layer.borderColor=(__bridge CGColorRef _Nullable)([UIColor whiteColor]);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnSave:(id)sender {
    NSString *urlString = @"https://simple-contact-crud.herokuapp.com/contact";
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:lbFirstName.text forKey:@"firstName"];
    [dictionary setObject:lbLastName.text forKey:@"lastName"];
    [dictionary setValue:lbAge.text forKey:@"age"];
    [dictionary setValue:@"http://vignette1.wikia.nocookie.net/lotr/images/6/68/Bilbo_baggins.jpg/revision/latest?cb=20130202022550" forKey:@"photo"];
    NSLog(@"data1:%@",dictionary);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    
    [request setHTTPMethod:@"POST"];
    
    [request setURL:url];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          NSString *responseStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
          NSLog(@"Data received: %@", responseStr);
          
          
      }] resume];
    
    [self displayMessage];
    

}

- (id)cleanJsonToObject:(id)data
{
    NSError* error;
    if (data == (id)[NSNull null])
    {
        return [[NSObject alloc] init];
    }
    id jsonObject;
    if ([data isKindOfClass:[NSData class]])
    {
        jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    } else
    {
        jsonObject = data;
    }
    if ([jsonObject isKindOfClass:[NSArray class]])
    {
        NSMutableArray *array = [jsonObject mutableCopy];
        for (int i = (int)array.count-1; i >= 0; i--)
        {
            id a = array[i];
            if (a == (id)[NSNull null])
            {
                [array removeObjectAtIndex:i];
            } else
            {
                array[i] = [self cleanJsonToObject:a];
            }
        }
        return array;
    } else if ([jsonObject isKindOfClass:[NSDictionary class]])
    {
        NSMutableDictionary *dictionary = [jsonObject mutableCopy];
        for(NSString *key in [dictionary allKeys])
        {
            id d = dictionary[key];
            if (d == (id)[NSNull null])
            {
                dictionary[key] = @"";
            } else
            {
                dictionary[key] = [self cleanJsonToObject:d];
            }
        }
        return dictionary;
    } else
    {
        return jsonObject;
    }
}

-(void)displayMessage{
    NSString *message = @"contact saved";
    UIAlertView *toast = [[UIAlertView alloc]
                          initWithTitle:nil
                          message:message
                          delegate:nil
                          cancelButtonTitle:nil
                          otherButtonTitles:nil, nil];
    
    toast.backgroundColor=[UIColor redColor];
    [toast show];
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [toast dismissWithClickedButtonIndex:0 animated:YES];
    });
}
@end
