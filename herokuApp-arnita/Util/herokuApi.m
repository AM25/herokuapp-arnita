//
//  herokuApi.m
//  herokuApp-arnita
//
//  Created by Arnita Martiana on 17/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import "herokuApi.h"
#import "AppDelegate.h"
#import "MyJSON.h"
#import "SharedPreferences.h"
#import "Configs.h"

@implementation herokuAPI


-(id)initWithData:(id<herokuAPIProtocol>)protocol{
    self = [super init];
    if(self){
        mProtocol = protocol;
        //                        mAppDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}

/*

-(void) doHttpPOSTQuery:(NSString*)url  andData:(NSString*)post{
    NSLog(@"doHttpPOSTQuery %@ with data=%@", url, post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%ld", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    //    [request setValue:header forHTTPHeaderField:@"Authorization"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField: @"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField: @"Accept"];
    // [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:60.0]; [request setHTTPBody:postData];
    
    responseData = [NSMutableData data];
    urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}

-(void) doHttpPOSTQueryHeader:(NSString*)url  andData:(NSString*)post{
    NSLog(@"doHttpPOSTQueryHeader %@ with data=%@", url, post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%ld", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    //    [request setValue:header forHTTPHeaderField:@"Authorization"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField: @"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField: @"Accept"];
    [request setValue:[SharedPreferences getdeviceUniqueID] forHTTPHeaderField:@"deviceUniqueID"];
    [request setValue:[SharedPreferences getAuthToken] forHTTPHeaderField:@"authToken"];
    
    
    [request setTimeoutInterval:60.0]; [request setHTTPBody:postData];
    
    responseData = [NSMutableData data];
    urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}

-(void)doHttpGETQUery:(NSString*)url andAuthHeader:(NSString*)header{
    NSLog(@"GET %@", url);
    if(header!=nil){
        NSLog(@"--header=%@", header);
    }
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    if(header!=nil){
        [request setValue:header forHTTPHeaderField:@"Authorization"];//change this according to your need.
    }
    //    [request setHTTPBody:postData];
    
    responseData = [NSMutableData data];
    urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}



#pragma mark -
#pragma mark NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [responseData setLength:0];
    
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    mRespondCode = [httpResponse statusCode];
    NSLog(@"didReceiveResponse code = %d", mRespondCode);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"connectionDidFinishLoading %@", error);
    
    responseData = nil;
    if(mProtocol!=nil){
        [mProtocol onHerokuAPIDataReturned:nil withRequestedType:0];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSLog(@"*********************************************************");
    NSLog(@"connectionDidFinishLoading %@", responseString);
    NSLog(@"*********************************************************");
    
    NSObject *data = nil;
    
    @try {
        
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:responseData
                                                                   options:NSJSONReadingMutableContainers error:nil];
        
        if(mRequestType==API_GET_CONTACTS){
            NSLog(@"---API_GET_CONTACTS");
            
            jsonObject = [responseString JSONValue];
            
            
            NSDictionary *dict1=[jsonObject valueForKeyPath:@"authenticationTokens"];
            NSDictionary *dict3=[dict1 valueForKey:@"abstractResponse"];
            NSDictionary *dict2=[dict1 valueForKey:@"authtokenResponse"];
            
            if (dict2 == (id)[NSNull null]) {
                
            } else {
                NSString *memberID              = dict2[@"memberId"];
                NSString *authToken             = dict2[@"authToken"];
                NSString *deviceUniqueId        = dict2[@"authDevice"];
                NSString *firstName             = dict2[@"firstName"];
                NSString *lastName              = dict2[@"lastName"];
                NSString *virtualAccount        = dict2[@"vaNumber"];
                NSString *point                 = dict2[@"point"];
                NSString *qrCodeImage           = dict2[@"qrImage"];
                NSString *userPic               = dict2[@"userPic"];
                NSString *ktp                   = dict2[@"idNumber"];
                NSString *gender                = dict2[@"gender"];
                NSString *birth                 = dict2[@"birthDate"];
                NSString *mother                = dict2[@"motherName"];
                NSString *birthPlace            = dict2[@"birthPlace"];
                NSString *phoneNumber           = dict2[@"phoneNumber"];
                NSString *email                 = dict2[@"email"];
                NSString *vaPermata             = dict2[@"vaNumber2"];
                NSString *isCorporateMember     = dict2[@"isCorporateMember"];
                
                
                
                if (firstName == (id)[NSNull null]) {
                    firstName = @"";
                    
                }
                
                if (ktp == (id)[NSNull null]) {
                    ktp = @"";
                    
                }
                
                if (gender == (id)[NSNull null]) {
                    gender = @"";
                    
                }
                
                if (birth == (id)[NSNull null]) {
                    birth = @"";
                    
                }
                
                if (mother == (id)[NSNull null]) {
                    mother = @"";
                    
                }
                
                if (birthPlace == (id)[NSNull null]) {
                    birthPlace = @"";
                    
                }
                
                
                if (virtualAccount != (id)[NSNull null]) {
                    [SharedPreferences setVaNumber:virtualAccount];
                }
                
                
                if (vaPermata != (id)[NSNull null]) {
                    [SharedPreferences setVaNumberPermata:vaPermata];
                }
                
                [SharedPreferences setAuthToken:authToken];
                [SharedPreferences setMemberId:memberID];
                [SharedPreferences setdeviceUniqueID:deviceUniqueId];
                [SharedPreferences setFirstName:firstName];
                [SharedPreferences setLastName:lastName];
                [SharedPreferences setPointBalance:point];
                [SharedPreferences setQRCodeMember:qrCodeImage];
                [SharedPreferences setKTP:ktp];
                [SharedPreferences setGender:gender];
                [SharedPreferences setBirthDate:birth];
                [SharedPreferences setMotherName:mother];
                [SharedPreferences setBirthPlace:birthPlace];
                [SharedPreferences setPhone:phoneNumber];
                [SharedPreferences setEmail:email];
                [SharedPreferences setCorporateMember:isCorporateMember];
                
                
                
                if (userPic != nil) {
                    [SharedPreferences setUserPic:@""];
                }
                
                
                
                if (userPic != nil)
                {
                    
                    if (![userPic isKindOfClass:[NSNull class]]) {
                        [SharedPreferences setUserPic:userPic];
                    }
                    
                    data = responseString;
                    
                }
            }
            
            NSString *responseStatusLogin  = dict3[@"responseStatus"];
            NSString *responseMessageLogin  = dict3[@"responseMessage"];
            [SharedPreferences setResponseRegister:responseStatusLogin];
            [SharedPreferences setResponseMessage:responseMessageLogin];
            data = responseString;
            
        }else if(mRequestType==API_SIGN_UP){
            NSLog(@"---API_SIGN_UP");
            jsonObject = [responseString JSONValue];
            
            NSArray *response = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[response valueForKey:@"responseStatus"];
            [SharedPreferences setResponseRegister:responseMessage];
            
            NSDictionary *dict1=[jsonObject valueForKeyPath:@"verificationCode"];
            NSDictionary *dict2=[jsonObject valueForKeyPath:@"memberId"];
            
            NSString *kode     =[NSString stringWithFormat:@"%@", dict1];
            NSString *memberID =[NSString stringWithFormat:@"%@", dict2];
            
            [SharedPreferences setKodeVerifikasi:kode];
            [SharedPreferences setMemberId:memberID];
            
            data = responseString;
            
        }else if(mRequestType==API_SIGN_UP_VERIFICATION){
            NSLog(@"---API_SIGN_UP_VERIFICATION");
            
            jsonObject = [responseString JSONValue];
            
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            [SharedPreferences setResponseRegister:responseMessage];
            
            data = responseString;
            
            
        }else if(mRequestType==API_FORGOT){
            NSLog(@"---API_FORGOT");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1=[jsonObject valueForKeyPath:@"code"];
            NSDictionary *dict2=[jsonObject valueForKeyPath:@"abstractResponse"];
            NSString *forgotCode     =[NSString stringWithFormat:@"%@", dict1];
            
            NSString *responseStatus=[dict2 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict2 valueForKey:@"responseMessage"];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            if ([responseStatus isEqualToString:@"INQ000"]) {
                [SharedPreferences setForgotCode:forgotCode];
            }
            
            
            data = responseString;
            
            
        }else if(mRequestType==API_RESET_PASSWORD){
            NSLog(@"---API_RESETPASSWORD");
            
            jsonObject = [responseString JSONValue];
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = responseString;
            
        }else if(mRequestType==API_CHECK_WALLET_BALANCE){
            NSLog(@"---API_CHECK_WALLET_BALANCE");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1=[jsonObject valueForKeyPath:@"pockets"];
            NSArray *dict2 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict2 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict2 valueForKey:@"responseMessage"];
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            NSString *walletBalance              = dict1[@"balance"];
            
            [SharedPreferences setWalletBalance:walletBalance];
            
            data = responseString;
            
            
        }else if(mRequestType==API_ADVERTISING_LIST){
            NSLog(@"---API_ADVERTISING_LIST");
            
            jsonObject = [responseString JSONValue];
            
            
            data = responseString;
            
        }else if(mRequestType==API_ADVERTISING_DETAILS){
            NSLog(@"---API_ADVERTISING_DETAILS");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict=[jsonObject valueForKeyPath:@"advertisingResponse"];
            NSArray *imageArray=[dict valueForKey:@"advertisingImage"];
            
            // Store it
            [[NSUserDefaults standardUserDefaults] setObject:imageArray forKey:@"imageArrayBanner"];
            NSArray *img=[NSArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"imageArrayBanner"]];
            [SharedPreferences setAdvertisingResponse:imageArray];
            
            data = imageArray;
            
        }else if(mRequestType==API_UV_BALANCE){
            NSLog(@"---API_UV_BALANCE");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1=[jsonObject valueForKeyPath:@"totalBalance"];
            NSString *dealsBalance         = [NSString stringWithFormat:@"%@", dict1];
            
            if (dealsBalance == (id)[NSNull null]) {
                dealsBalance = @"0";
                
            }
            
            if (dealsBalance != nil)
            {
                
                if (![dealsBalance isKindOfClass:[NSNull class]]) {
                    [SharedPreferences setUVBalance:dealsBalance];
                }
                
                data = responseString;
                
            }
            
            [SharedPreferences setUVBalance:dealsBalance];
            
            data = responseString;
            
        }else if(mRequestType==API_GETSTORELIST){
            NSLog(@"---API_STORELIST");
            
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            [SharedPreferences setResponseRegister:responseMessage];
            NSError *error;
            StoreResponse *storeModel = [[StoreResponse alloc] initWithString:responseString error:&error];
            
            
            data = storeModel;
            
        }else if(mRequestType == API_SHARE_VOUCHERKU){
            NSLog(@"---API_SHARE_VOUCHERKU");
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject valueForKey:@"deals"];
            NSArray *dict3 = [dict2 valueForKey:@"dealsDetail"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            [SharedPreferences setResponseRegister:responseMessage];
            
            // Store it
            [[NSUserDefaults standardUserDefaults] setObject:dict3 forKey:@"dealsDetail"];
            
            data = responseMessage;
            
            NSError *error;
            VoucherResponseModel *VoucherModel = [[VoucherResponseModel alloc] initWithString:responseString error:&error];
            
            data = VoucherModel;
        }else if(mRequestType == API_UPLOAD_PHOTO){
            NSLog(@"---API_UPLOAD_PHOTO");
            
            
            if(mRespondCode==200){
                data = responseString;
            }else{
                data = nil;
            }
            
            
        }else if(mRequestType == API_RESET_PASSWORD){
            
            jsonObject = [responseString JSONValue];
            
            data = responseString;
            
        }else if(mRequestType == API_VOUCHERKU_FISIK){
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject objectForKey:@"storeResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            
            [SharedPreferences setResponseRegister:responseMessage];
            
            NSError *error;
            storeResponse *FisikModel = [[storeResponse alloc] initWithString:responseString error:&error];
            
            
            data = FisikModel;
            
        }else if(mRequestType == API_VOUCHERKU_DIGITAL){
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject objectForKey:@"deals"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            
            
            [SharedPreferences setResponseRegister:responseMessage];
            
            NSError *error;
            storeResponse *DigitalModel = [[storeResponse alloc] initWithString:responseString error:&error];
            
            
            data = DigitalModel;
            
        }else if(mRequestType == API_LOGOUT){
            NSLog(@"---API_LOGOUT");
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            [SharedPreferences setResponseRegister:responseMessage];
            
            
            data = responseString;
            
            
        } else if(mRequestType == API_SHARING_BALANCE){
            NSLog(@"---API_SHARING_BALANCE");
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            [SharedPreferences setResponseRegister:responseMessage];
            
            
            data = responseString;
        } else if (mRequestType == API_DEAL_SHARE){
            NSLog(@"---API_DEAL_SHARE");
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage1=[dict1 valueForKey:@"responseMessage"];
            [SharedPreferences setResponseRegister:responseMessage];
            [SharedPreferences setResponseMessage:responseMessage1];
            
            data = responseString;
        } else if(mRequestType==API_AUTOREGISTER){
            NSLog(@"---API_AUTOREGISTER");
            jsonObject = [responseString JSONValue];
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            [SharedPreferences setResponseRegister:responseMessage];
            NSDictionary *dict2=[jsonObject valueForKeyPath:@"memberId"];
            
            NSString *memberID =[NSString stringWithFormat:@"%@", dict2];
            [SharedPreferences setMemberIdTo:memberID];
            // Store it
            [[NSUserDefaults standardUserDefaults] setObject:dict2 forKey:@"memberIdTo"];
            
            data = responseString;
            
            
            
        }else if(mRequestType == API_GETSTOREDETAIL){
            NSLog(@"---API_STORELIST_DETAIL");
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSDictionary *dict2=[jsonObject valueForKey:@"products"];
            
            if (dict2 == (id)[NSNull null]) {
                
                NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
                [SharedPreferences setResponseRegister:responseMessage];
                
                
            } else {
                
                NSError *error;
                StoreDetailResponseModel *StoreDetailModel = [[StoreDetailResponseModel alloc] initWithString:responseString error:&error];
                NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
                [SharedPreferences setResponseRegister:responseMessage];
                
                data = StoreDetailModel;
            }
            
        }else if(mRequestType == API_ORDERCART){
            NSLog(@"---API_ORDERCART");
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            NSError *error;
            CartProductResponseModel *ProductModel = [[CartProductResponseModel alloc] initWithString:responseString error:&error];
            
            CartModel *cart = [ProductModel valueForKey:@"orders"];
            
            NSString *orderNumber = [[[cart valueForKey:@"orderNumber"]allObjects]objectAtIndex:0];
            NSString *orderId = [[[cart valueForKey:@"orderId"]allObjects]objectAtIndex:0];
            NSString *totalAmount = [[[cart valueForKey:@"totalAmount"]allObjects]objectAtIndex:0];
            NSString *taxAmount = [[[cart valueForKey:@"taxAmount"]allObjects]objectAtIndex:0];
            NSString *discountValue = [[[cart valueForKey:@"discountValue"]allObjects]objectAtIndex:0];
            
            if([[SharedPreferences getOrderNumber]length] == 0){
                
                NSLog(@"TIDAK ADA ORDER NUMBER VC API");
                
                [SharedPreferences setOrderNumber:orderNumber];
                [SharedPreferences setOrderId:orderId];
                [SharedPreferences setTotalAmount:totalAmount];
                [SharedPreferences setTaxAmount:taxAmount];
                [SharedPreferences setDiscount:discountValue];
                [SharedPreferences setOrderId:orderId];
                
            }
            
            data = ProductModel;
            
        }else if(mRequestType == API_CARTLIST){
            NSLog(@"---API_CARTLIST");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *dict2 = [jsonObject valueForKey:@"totalTrxAmount"];
            NSArray *dict3 = [jsonObject valueForKey:@"highestVoucherClass"];
            NSArray *dict4 = [jsonObject valueForKey:@"orders"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            NSString* higher= [NSString stringWithFormat:@"%@",dict3];
            NSString *total=[jsonObject valueForKey:@"totalAmount"];
            NSString *totalTrx=[jsonObject valueForKey:@"totalTrxAmount"];
            
            if (dict4==NULL) {
                
                NSString *badgeCount = @"0";
                [SharedPreferences setBadge:badgeCount];
                [SharedPreferences setOrderNumber:@""];
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"addressId"];
                
            }else{
                NSString *badgeCount = [NSString stringWithFormat:@"%lu",[dict4 count]];
                [SharedPreferences setBadge:badgeCount];
                
                NSString* orderId = [[dict4 valueForKey:@"orderId"]objectAtIndex:0];
                NSString* orderNumber = [[dict4 valueForKey:@"orderNumber"]objectAtIndex:0];
                NSString* merchantId = [[dict4 valueForKey:@"merchantId"]objectAtIndex:0];
                NSString* storeId = [[dict4 valueForKey:@"storeId"]objectAtIndex:0];
                
                //        filter array with quantity is not 0
                NSArray *filteredData = [dict4 filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(deliveryMethod == %@)", @-1]];
                
                NSString* deliveryMethod = [[dict4 valueForKey:@"deliveryMethod"]objectAtIndex:0];
                NSString* addressId = [[dict4 valueForKey:@"deliveryAddressId"]objectAtIndex:0];
                
                
                [SharedPreferences setOrderId:orderId];
                [SharedPreferences setOrderNumber:orderNumber];
                [SharedPreferences setMerchantId:merchantId];
                [SharedPreferences setStoreId:storeId];
                [SharedPreferences setTotalAmount:[NSString stringWithFormat:@"%@",total]];
                
                
                
                if (addressId != (id)[NSNull null]) {
                    
                    
                    NSString* address = [[dict4 valueForKey:@"deliveryAddress"]objectAtIndex:0];
                    NSString* village = [[dict4 valueForKey:@"village"]objectAtIndex:0];
                    NSString* district = [[dict4 valueForKey:@"district"]objectAtIndex:0];
                    NSString* city = [[dict4 valueForKey:@"city"]objectAtIndex:0];
                    NSString* province = [[dict4 valueForKey:@"province"]objectAtIndex:0];
                    NSString* postalCode = [[dict4 valueForKey:@"postalCode"]objectAtIndex:0];
                    
                    [SharedPreferences setAddressId:addressId];
                    [SharedPreferences setLine:address];
                    [SharedPreferences setVillage:village];
                    [SharedPreferences setDistrict:district];
                    [SharedPreferences setCity:city];
                    [SharedPreferences setProvince:province];
                    [SharedPreferences setPostalCode:postalCode];
                    
                }
            }
            
            
            NSError *error;
            CartResponseModel *CartModel = [[CartResponseModel alloc] initWithString:responseString error:&error];
            
            [SharedPreferences setResponseRegister:responseMessage];
            
            [SharedPreferences setHigherClass:[NSString stringWithFormat:@"%@",dict3]];
            
            
            data = CartModel;
            
        }else if(mRequestType == API_GETADDRESS){
            NSLog(@"---API_GETADDRESS");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *arrayAddress = [jsonObject valueForKey:@"deliveryAddress"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            
            NSError *error;
            addressResponseModel *addressModel = [[addressResponseModel alloc] initWithString:responseString error:&error];
            
            [SharedPreferences setResponseRegister:responseMessage];
            
            data = addressModel;
            
        }else if(mRequestType == API_PAYMENT){
            NSLog(@"---API_PAYMENT");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            
            NSError *error;
            paymentResponseModel *paymentModel = [[paymentResponseModel alloc] initWithString:responseString error:&error];
            
            [SharedPreferences setResponseRegister:responseMessage];
            
            data = paymentModel;
            
            
        }else if(mRequestType == API_VIRTUALACCOUNT){
            NSLog(@"---API_VIRTUALACCOUNT");
            
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            [SharedPreferences setResponseRegister:responseMessage];
            
            NSError *error;
            VAResponseModel *VModel = [[VAResponseModel alloc] initWithString:responseString error:&error];
            
            
            VAModel *cart = [VModel valueForKey:@"orders"];
            NSString *orderNum = [[[cart valueForKey:@"orderNumber"]allObjects]objectAtIndex:0];
            NSString *virtual = [[[cart valueForKey:@"vaNumber"]allObjects]objectAtIndex:0];
            
            [SharedPreferences setVirtualAccount:virtual];
            [SharedPreferences setOrderNumber:orderNum];
            
            data = VModel;
            
            
        }else if(mRequestType == API_GETREWARD){
            NSLog(@"---API_GETREWARD");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            
            NSError *error;
            rewardResponseModel *rewardModel = [[rewardResponseModel alloc] initWithString:responseString error:&error];
            
            [SharedPreferences setResponseRegister:responseMessage];
            
            data = rewardModel;
            
            
        }else if(mRequestType == API_REWARDBYID){
            NSLog(@"---API_REWARDBYID");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            
            NSError *error;
            rewardResponseModel *rewardModel = [[rewardResponseModel alloc] initWithString:responseString error:&error];
            
            [SharedPreferences setResponseRegister:responseMessage];
            
            data = rewardModel;
            
        }else if(mRequestType == API_REDEEMREWARD){
            NSLog(@"---API_REDEEMREWARD");
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *detailResponses = [jsonObject valueForKey:@"detailResponses"];
            NSString *responsetatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            [SharedPreferences setResponseRegister:responsetatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = responseString;
            
            
        }else if(mRequestType == API_REWARDKU){
            NSLog(@"---API_REWARDKU");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject valueForKey:@"detailResponses"];
            NSString *responsetatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            [SharedPreferences setResponseRegister:responsetatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data=responseString;
            
        }else if(mRequestType == API_HISTORYVOUCHER){
            NSLog(@"---API_HISTORYVOUCHER");
            
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject objectForKey:@"deals"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            
            NSError *error;
            
            HistoryVoucherResponseModel *HistoryVoucherModel = [[HistoryVoucherResponseModel alloc] initWithString:responseString error:&error];
            
            [SharedPreferences setResponseRegister:responseMessage];
            
            data = HistoryVoucherModel;
            
        }else if(mRequestType == API_HISTORYREWARD){
            NSLog(@"---API_HISTORYREWARD");
            
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            
            NSError *error;
            HistoryRewardResponseModel *HistoryRewardModel = [[HistoryRewardResponseModel alloc] initWithString:responseString error:&error];
            
            [SharedPreferences setResponseRegister:responseMessage];
            
            data = HistoryRewardModel;
            
        }
        else if (mRequestType == API_CHECK_MEMBER_EXIST){
            NSLog(@"---API_CHECK_MEMBER_EXIST");
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            if ([responseStatus isEqualToString:@"INQ000"]) {
                NSArray *dict2 = [jsonObject valueForKey:@"registeredMember"];
                NSArray *dict3 = [jsonObject valueForKey:@"unregisteredMember"];
                
                if ([dict2 count]>0) {
                    NSArray *dict4=[dict2 objectAtIndex:0];
                    NSString *memberIdTo = [dict4 valueForKey:@"memberId"];
                    [SharedPreferences setMemberIdTo:memberIdTo];
                }else{
                    
                    [SharedPreferences setMemberIdTo:@""];
                }
                
                
            }
            
            data = responseString;
            
        }else if(mRequestType == API_DISCARDORDER){
            NSLog(@"---API_DISCARDORDER");
            
            
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            [SharedPreferences setResponseRegister:responseMessage];
            data = responseString;
            
        }else if(mRequestType==API_UBAHPROFILE){
            NSLog(@"---API_UBAHPROFILE");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            [SharedPreferences setResponseRegister:responseMessage];
            
            data = responseString;
            
        }else if(mRequestType==API_UBAHPASSWORD){
            NSLog(@"---API_UBAHPASSWORD");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = responseString;
            
        }else if(mRequestType==API_ADDADRESS){
            NSLog(@"---API_ADDADRESS");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseMessage=[dict1 valueForKey:@"responseStatus"];
            [SharedPreferences setResponseRegister:responseMessage];
            
            data = responseString;
            
        }
        else if(mRequestType==API_OTPPASSWORD){
            NSLog(@"---API_OTPPASSWORD");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *dict2= [jsonObject valueForKey:@"transactionId"];
            
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            if([[SharedPreferences getResponseRegister]isEqualToString:@"OD005"]){
                [SharedPreferences setTransId:dict2];
            }
            
            
            data = responseString;
            
        }else if(mRequestType==API_OTPPAYCONFIRMED){
            NSLog(@"---API_OTPPAYCONFIRMED");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responsetatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            [SharedPreferences setResponseRegister:responsetatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            NSArray *orders =[[jsonObject valueForKey:@"orders"] objectAtIndex:0];
            NSString *refCode =[orders valueForKey:@"paymentReferenceCode"];
            [SharedPreferences setRefCode:refCode];
            
            
            data = responseString;
            
        }else if(mRequestType==API_WEBVIEWCC){
            NSLog(@"---API_WEBVIEWCC");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *dict2 = [jsonObject valueForKey:@"url"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            [SharedPreferences setWebURL:dict2];
            
            
            data = responseString;
            
        }else if(mRequestType==API_ISSUEPOINT){
            NSLog(@"---API_ISSUEPOINT");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2=[jsonObject valueForKey:@"promotionMessageResponse"];
            if(dict2!=nil || ![dict2 isKindOfClass:[NSNull class]]){
                NSArray *dict3 = [dict2 valueForKey:@"promotionNumberIssued"];
                NSString *poin=[[dict3 valueForKey:@"basicRulePoint"]objectAtIndex:0];
                [SharedPreferences setPoin:poin];
            }
            
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            
            data = responseString;
            
        }else if(mRequestType==API_ISSUEPOINTMED){
            NSLog(@"---API_ISSUEPOINTMED");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject valueForKey:@"promotionMessageResponse"];
            
            //            NSArray *dict3 = [dict2 valueForKey:@"promotionNumberIssued"];
            //            NSString *poin = [[dict3 valueForKey:@"basicRulePoint"]objectAtIndex:0];
            
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = responseString;
            
        }else if(mRequestType==API_LISTPAYDIGITAL){
            NSLog(@"---API_LISTPAYDIGITAL");
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject objectForKey:@"deals"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            
            [SharedPreferences setResponseRegister:responseMessage];
            
            
            NSError *error;
            UltraVoucherResponse *ultraModel = [[storeResponse alloc] initWithString:responseString error:&error];
            
            
            data = ultraModel;
            
            
        }else if(mRequestType==API_PAYDIGITAL){
            NSLog(@"---API_PAYDIGITAL");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = responseString;
        }else if(mRequestType == API_HISTORYTOPUPWALLET){
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            NSError *error;
            HistoryWalletResponse *HistoryWalletModel = [[HistoryWalletResponse alloc] initWithString:responseString error:&error];
            
            data = HistoryWalletModel;
            
        }else if(mRequestType == API_AMOUNTSHARE){
            NSLog(@"---API_AMOUNTSHARE");
            
            
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject valueForKey:@"sharingBalance"];
            NSArray *voucherValue =[dict2 valueForKey:@"voucherValue"];
            
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *qty = [dict2 valueForKey:@"totalQuantity"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            if ([responseStatus isEqualToString:@"OD904"]) {
                
            }else{
                [SharedPreferences setAmountSharingList:voucherValue];
            }
            
            
            NSError *error;
            AmountResponse *AmountModel = [[AmountResponse alloc] initWithString:responseString error:&error];
            
            data = AmountModel;
            
        }else if(mRequestType==API_ADVERTISING_DETAILS_SPLASH){
            NSLog(@"---API_ADVERTISING_DETAILS_SPLASH");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict=[jsonObject valueForKeyPath:@"advertisingResponse"];
            NSArray *imageArray=[dict valueForKey:@"advertisingImage"];
            
            // Store it
            [[NSUserDefaults standardUserDefaults] setObject:imageArray forKey:@"imageArraySplashPromo"];
            NSArray *img=[NSArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"imageArraySplashPromo"]];
            
            [SharedPreferences setSplashResponse:imageArray];
            
            data = imageArray;
            
        }else if (mRequestType == API_PAYULTRAVOUCHER){
            
            NSLog(@"---API_PAYULTRAVOUCHER");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = responseString;
            
        }else if(mRequestType == API_GET_JNE_DESTINATION){
            NSLog(@"---API_GET_JNE_DESTINATION");
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject objectForKey:@"listCityCode"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            if ([responseStatus isEqualToString:@"JNE001"]) {
                NSString *destinationCode = [[dict2 valueForKey:@"code"]objectAtIndex:0];
                [SharedPreferences setDestinationCode:destinationCode];
                
            }
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            [SharedPreferences setArrayListCityCode:dict2];
            
            
            data = responseString;
            
            
        }else if(mRequestType == API_CHECK_JNE_RATES){
            NSLog(@"---API_CHECK_JNE_RATES");
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject objectForKey:@"listPackage"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            [SharedPreferences setArrayListJnePackage:[dict2 valueForKey:@"serviceDisplay"]];
            [SharedPreferences setArrayListJnePrice:[dict2 valueForKey:@"price"]];
            
            NSError *error;
            JNERatesModel *jneRatesModel = [[JNERatesModel alloc] initWithString:responseString error:&error];
            
            data = jneRatesModel;
            
        }
        else if(mRequestType == API_SHARING_INQ){
            NSLog(@"---API_SHARING_INQ");
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSDictionary *dict2 = [[jsonObject valueForKey:@"sharingBalance"]objectAtIndex:0];
            NSArray *details = [[dict2 valueForKey:@"details"]objectAtIndex:0];
            NSString *voucherCode =[details valueForKey:@"voucherCode"];
            NSString *voucherId =[details valueForKey:@"voucherId"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            [SharedPreferences setVoucherCode:voucherCode];
            [SharedPreferences setVoucherId:voucherId];
            
            NSError *error;
            SharingInquiryResponse *SharInqModel = [[SharingInquiryResponse alloc] initWithString:responseString error:&error];
            
            
            data = SharInqModel;
            
            
        }else if (mRequestType == API_REWARDKU_LIST){
            
            NSLog(@"---API_REWARDKU_LIST");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *detailResponses = [jsonObject valueForKey:@"detailResponses"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            
            NSError *error;
            RewardKuResponseModel *rewardKuModel = [[RewardKuResponseModel alloc] initWithString:responseString error:&error];
            
            data = rewardKuModel;
            
        }else if(mRequestType == API_UPDATE_DELIVERY_ADDRESS){
            NSLog(@"---API_UPDATE_DELIVERY_ADDRESS");
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *orders = [jsonObject valueForKey:@"orders"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            
            data = responseString;
            
        }else if(mRequestType == API_UPDATE_DELIVERY_ADDRESS1){
            NSLog(@"---API_UPDATE_DELIVERY_ADDRESS1");
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *orders = [jsonObject valueForKey:@"orders"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            if ([responseStatus isEqualToString:@"OD002"]) {
                NSString *orderno=[[orders valueForKey:@"orderNumber"]objectAtIndex:0];
                
            }
            
            
            data = responseString;
            
        }else if(mRequestType == API_UVBALANCE_VOUCHER_LIST){
            NSLog(@"---API_UVBALANCE_VOUCHER_LIST");
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            NSError *error;
            UVBalanceVoucherkuResponse *UVBalanceVoucher = [[UVBalanceVoucherkuResponse alloc] initWithString:responseString error:&error];
            
            
            data = UVBalanceVoucher;
            
        }else if(mRequestType == API_STATUS_PEMBAYARAN){
            NSLog(@"---API_STATUS_PEMBAYARAN");
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            NSError *error;
            OrderStatusHistoryResponseModel *orderStatus = [[OrderStatusHistoryResponseModel alloc] initWithString:responseString error:&error];
            
            
            data = orderStatus;
            
        }else if(mRequestType == API_MEMBER_POINT){
            
            NSLog(@"---API_MEMBER_POINT");
            
            jsonObject = [responseString JSONValue];
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *point = [jsonObject valueForKey:@"totalPoints"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            [SharedPreferences setPointBalance:point];
            
            
            data = responseString;
            
        }else if(mRequestType==API_PROVINCELIST){
            NSLog(@"---API_PROVINCELIST");
            
            jsonObject = [responseString JSONValue];
            
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            NSDictionary *provinceList = [jsonObject valueForKey:@"utilMessageResponse"];
            NSArray *prov=[provinceList valueForKey:@"label"];
            NSArray *provId=[provinceList valueForKey:@"id"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            [SharedPreferences setProvinceNameList:prov];
            [SharedPreferences setProvinceIdList:provId];
            
            NSError *error;
            ProvinceResponse *provinsi = [[ProvinceResponse alloc] initWithString:responseString error:&error];
            
            
            data = provinsi;
            
        }else if(mRequestType==API_CITYLIST){
            NSLog(@"---API_CITYLIST");
            
            jsonObject = [responseString JSONValue];
            
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSDictionary *cityList = [jsonObject valueForKey:@"utilMessageResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            NSArray *city=[cityList valueForKey:@"label"];
            NSArray *cityId=[cityList valueForKey:@"id"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            [SharedPreferences setCityList:city];
            [SharedPreferences setCityIdList:cityId];
            
            data = responseString;
            
        }else if(mRequestType==API_DISTRICTLIST){
            NSLog(@"---API_DISTRICTLIST");
            
            jsonObject = [responseString JSONValue];
            
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSDictionary *districtList = [jsonObject valueForKey:@"utilMessageResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            NSArray *district=[districtList valueForKey:@"label"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            [SharedPreferences setDistrictList:district];
            
            
            data = responseString;
            
        }else if(mRequestType==API_REDEEM_VOUCHER_DIGITAL){
            NSLog(@"---API_REDEEM_VOUCHER_DIGITAL");
            
            jsonObject = [responseString JSONValue];
            
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSDictionary *voucherResponse = [jsonObject valueForKey:@"voucherResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = responseString;
            
        }else if(mRequestType==API_REDEEMREWARD){
            NSLog(@"---API_REDEEMREWARD");
            
            jsonObject = [responseString JSONValue];
            
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *detailResponses = [jsonObject valueForKey:@"detailResponses"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = responseString;
            
        }else if(mRequestType==API_VOUCHER_REDEEM){
            NSLog(@"---API_VOUCHER_REDEEM");
            
            jsonObject = [responseString JSONValue];
            
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = responseString;
            
        }else if(mRequestType==API_REWARD_REDEEM_NEW){
            NSLog(@"---API_REWARD_REDEEM_NEW");
            
            jsonObject = [responseString JSONValue];
            
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = responseString;
            
        }else if(mRequestType==API_BILLER_AMOUNT_FEE_PRODUCT){
            NSLog(@"---API_BILLER_AMOUNT_FEE_PRODUCT");
            
            jsonObject = [responseString JSONValue];
            
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            NSArray *active= [jsonObject objectForKey:@"active"];
            NSArray *fee= [jsonObject valueForKey:@"fee"];
            NSMutableArray *arrAmount = [jsonObject valueForKey:@"amount"];
            NSArray *amount= [jsonObject valueForKey:@"amount"];
            NSArray *productValue= [jsonObject valueForKey:@"productValue"];
            NSString *idPlnPost = [active valueForKey:@"idPlnPost"];
            NSLog(@"arrAmount:%@", amount);
            NSLog(@"arrProdValue:%@", productValue);
            NSLog(@"arractive:%@", active);
            NSLog(@"arrfee:%@", fee);
            
            // Store it
            [[NSUserDefaults standardUserDefaults] setObject:active forKey:@"ArrayBillerActive"];
            [[NSUserDefaults standardUserDefaults] setObject:fee forKey:@"ArrayBillerFee"];
            [[NSUserDefaults standardUserDefaults] setObject:arrAmount forKey:@"ArrayBillerAmount"];
            [[NSUserDefaults standardUserDefaults] setObject:productValue forKey:@"ArrayBillerProductValue"];
            
            [SharedPreferences setArrayBillerActive:active];
            
            data = responseString;
            
        }else if(mRequestType==API_BILLER_CHECK){
            NSLog(@"---API_BILLER_CHECK");
            
            jsonObject = [responseString JSONValue];
            
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            NSString *nama=[jsonObject valueForKey:@"name"];
            NSString *tagihan=[jsonObject valueForKey:@"price"];
            NSString *fee=[jsonObject valueForKey:@"fee"];
            NSString *totalAmount=[jsonObject valueForKey:@"totalAmount"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            if ([responseStatus isEqualToString:@"BL001"]) {
                [SharedPreferences setPrice:tagihan];
                [SharedPreferences setFee:fee];
                [SharedPreferences setNamaPelanggan:nama];
                [SharedPreferences setTotalAmount:totalAmount];
            }
            
            
            data = responseString;
        }else if(mRequestType==API_BILLER_INQUIRY){
            NSLog(@"---API_BILLER_INQUIRY");
            
            jsonObject = [responseString JSONValue];
            
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            NSString *transactionId=[jsonObject valueForKey:@"transactionId"];
            NSString *va=[jsonObject valueForKey:@"vaNumber"];
            
            NSString *totalAmount=[jsonObject valueForKey:@"totalAmount"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            [SharedPreferences setTransId:transactionId];
            
            if (![va isEqual:[NSNull null]]) {
                [SharedPreferences setVirtualAccount:va];
            }
            
            if (![totalAmount isEqual:[NSNull null]]) {
                [SharedPreferences setTotalAmount:totalAmount];
            }
            
            data = responseString;
        }else if(mRequestType==API_BILLER_PAYMENT_WALLET){
            NSLog(@"---API_BILLER_PAYMENT_WALLET");
            
            jsonObject = [responseString JSONValue];
            
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus = [dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage = [dict1 valueForKey:@"responseMessage"];
            NSString *token = [jsonObject valueForKey:@"token"];
            NSString *transId = [jsonObject valueForKey:@"transactionId"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            if ([responseStatus isEqualToString:@"BL002"]) {
                if ([token isEqual: [NSNull null]]) {
                    NSLog(@"token null");
                    [SharedPreferences setTransId:transId];
                    [SharedPreferences setToken:@""];
                }else{
                    [SharedPreferences setTransId:transId];
                    [SharedPreferences setToken:token];
                }
            }
            
            
            data = responseString;
        }
        else if(mRequestType==API_BILLER_FAVORITE){
            NSLog(@"---API_BILLER_FAVORITE");
            
            jsonObject = [responseString JSONValue];
            
            
            NSArray *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            NSArray *dict2 = [jsonObject valueForKey:@"contractFavorite"];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            [SharedPreferences setFavoriteList:dict2];
            
            
            data = responseString;
        }else if(mRequestType == API_HISTORY_BILLER_VOUCHER){
            NSLog(@"---API_HISTORY_BILLER_VOUCHER");
            
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject objectForKey:@"orderHistory"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            NSError *error;
            
            HistoryBillerVoucherResponseModel *HistoryBillerVoucherModel = [[HistoryBillerVoucherResponseModel alloc] initWithString:responseString error:&error];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = HistoryBillerVoucherModel;
            
        }else if(mRequestType == API_HISTORY_BILLER_DETAIL){
            NSLog(@"---API_HISTORY_BILLER_DETAIL");
            
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject objectForKey:@"billerHistory"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            NSError *error;
            
            HistoryBillerDetailResponseModel *HistoryBillerDetailModel = [[HistoryBillerDetailResponseModel alloc] initWithString:responseString error:&error];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = HistoryBillerDetailModel;
            
        }else if(mRequestType == API_HISTORY_VOUCHER_DETAIL){
            NSLog(@"---API_HISTORY_VOUCHER_DETAIL");
            
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject objectForKey:@"orderVouchers"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            NSError *error;
            
            HistoryVoucherDetailResponseModel *HistoryVoucherDetailModel = [[HistoryVoucherDetailResponseModel alloc] initWithString:responseString error:&error];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = HistoryVoucherDetailModel;
            
        }else if(mRequestType == API_PAYMENT_ULTRAVOUCHER_AMOUNT){
            NSLog(@"---API_PAYMENT_ULTRAVOUCHER_AMOUNT");
            
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *dict2 = [jsonObject objectForKey:@"vouchers"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            NSError *error;
            
            PaymentUltraVoucherResponse *PaymentUltraVoucher = [[PaymentUltraVoucherResponse alloc] initWithString:responseString error:&error];
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = PaymentUltraVoucher;
            
        }else if(mRequestType==API_PAYMENTULTRAVOUCHER){
            NSLog(@"---API_PAYMENTULTRAVOUCHER");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSArray *orders = [jsonObject valueForKey:@"orders"];
            NSString *orderNo = [orders valueForKey:@"orderNumber"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = responseString;
            
        }else if(mRequestType == API_FIREBASE_NOTIFICATION){
            NSLog(@"---API_FIREBASE_NOTIFICATION");
            
            jsonObject = [responseString JSONValue];
            NSDictionary *dict1 = [jsonObject valueForKey:@"abstractResponse"];
            NSString *responseStatus=[dict1 valueForKey:@"responseStatus"];
            NSString *responseMessage=[dict1 valueForKey:@"responseMessage"];
            
            
            [SharedPreferences setResponseRegister:responseStatus];
            [SharedPreferences setResponseMessage:responseMessage];
            
            data = responseString;
            
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"error-----");
    }
    
    
    @finally {
        
    }
    
    if(mProtocol!=nil){
        NSLog(@"mProtocol oneVoucherAPIDataReturned");
        [mProtocol oneVoucherAPIDataReturned:data withRequestedType:mRequestType];
    }
}

-(void)doLogin:(NSString *)deviceUniqueId andUsername:(NSString *)username andPassword:(NSString *)password{
    mRequestType = API_LOGIN;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:deviceUniqueId,username,password, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"deviceUniqueId",@"username",@"password", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQuery:URL_LOGIN andData:myString];
}


-(void)doSignUp:(NSString *)deviceUniqueId withUsername:(NSString *)username withPass:(NSString *)password withPhone:(NSString *)phone withFirstname:(NSString *)firstname withLastname:(NSString *)lastname withCopMember:(NSString *)copMember {
    mRequestType = API_SIGN_UP;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:deviceUniqueId,username,password,phone,firstname,lastname,copMember, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"deviceUniqueId",@"username",@"password",@"phoneNumber",@"firstName",@"lastName",@"isCorporateMember", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    //    NSDictionary *finalJsonData = [[NSDictionary alloc] initWithObjectsAndKeys:tempJsonData, nil, nil];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQuery:URL_REGISTER andData:myString];
}

-(void)doVerifikasi:(NSString *)username andCode:(NSString *)code{
    mRequestType = API_SIGN_UP_VERIFICATION;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:username,code, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"username",@"code", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQuery:URL_SIGN_UP_VERIFICATION andData:myString];
}

-(void)getForgotPasswordCode:(NSString *)email{
    mRequestType = API_FORGOT;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:email, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"email", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQuery:URL_FORGOT andData:myString];
}

-(void)getWalletBalance:(NSString *)memberId{
    mRequestType = API_CHECK_WALLET_BALANCE;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_CHECK_WALLET_BALANCE andData:myString];
    
}

-(void)getAdvertisingList{
    mRequestType = API_ADVERTISING_LIST;
    
    [self doHttpPOSTQuery:URL_ADVERTISING_LIST andData:@""];
}

-(void)getAdvertisingDetails:(NSString *)advertisingType andArticleCategory:(NSString *)articleCategory andCityId:(NSString *)cityId{
    mRequestType = API_ADVERTISING_DETAILS;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:advertisingType, articleCategory, cityId, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"advertisingType",@"articleCategory",@"cityId", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQuery:URL_ADVERTISING_DETAILS andData:myString];
}

-(void)getUVBalance:(NSString *)memberId andVoucherClass:(NSString *)voucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords{
    
    mRequestType = API_UV_BALANCE;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,voucherClass,page, nRecords, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"voucherClass",@"page",@"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_UV_BALANCE andData:myString];
}

-(void)GetStoreList:(NSString *)keyword withorder:(NSString *)order withpage:(NSString *)page withnRecords:(NSString *)nRecords withstoreType:(NSString *)storeType withmerchantId:(NSString *)merchantId withcityId:(NSString *)cityId withnearBy:(NSString *)nearBy withlatitude:(NSString *)latitude withLongitude:(NSString *)longitude withVoucherClass:(NSString *)sVoucherClass{
    
    
    mRequestType                    = API_GETSTORELIST;
    NSArray *objects                = [[NSArray alloc] initWithObjects:keyword,order,page,nRecords,storeType,merchantId,cityId,nearBy,latitude,longitude,sVoucherClass,nil];
    NSArray *keys                   = [[NSArray alloc] initWithObjects:@"keyword",@"order",@"page",@"nRecords",@"storeType",@"merchantId",@"cityId",@"nearBy",@"latitude",@"longitude",@"sVoucherClass", nil];
    NSDictionary *tempJsonData      = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    [self doHttpPOSTQueryHeader:URL_GETSTORE andData:myString];
    
}

-(void)getVoucherKu:(NSString *)memberId andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecorsd:(NSString *)nRecords{
    
    mRequestType = API_SHARE_VOUCHERKU;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,sVoucherClass,page,nRecords, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"sVoucherClass",@"page",@"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_POCKET_DEALS_DIGITAL andData:myString];
}

-(void)doUploadPhoto:(UIImage*)image{
    mRequestType = API_UPLOAD_PHOTO;
    
    NSLog(@"doUploadPhoto url=%@", URL_UPLOAD_PHOTO);
    
    NSURL* requestURL = [NSURL URLWithString:URL_UPLOAD_PHOTO];
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"file";
    
    // the server url to which the image (or the media) is uploaded. Use your server url here
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add image data
    if(image){
        NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", FileParamConstant,@"image.jpg"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"memberId\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]]; //Key for your parameter to send
        [body appendData:[[SharedPreferences getMemberID] dataUsingEncoding:NSUTF8StringEncoding]]; //Add your parameter value here
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    
    responseData = [NSMutableData data];
    urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}


-(void)doAutoRegister:(NSString *)creator andUsername:(NSString *)username andFirstName:(NSString *)firstName andLastName:(NSString *)lastName andMobileNumber:(NSString *)mobileNumber{
    mRequestType = API_AUTOREGISTER;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:creator, username, firstName, lastName, mobileNumber, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"creator",@"username",@"firstName",@"lastName",@"mobileNumber", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQuery:URL_AUTOREGISTER andData:myString];
    
}

-(void)doForgetPasswordWithUsername:(NSString *)username andVerificationCode:(NSString *)verificationCode andNewPassword:(NSString *)password{
    mRequestType = API_RESET_PASSWORD;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:username,verificationCode,password, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"username",@"verificationCode",@"password", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_RESETPASSWORD andData:myString];
}

-(void)getVoucherKuFisik:(NSString *)memberId andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords{
    
    mRequestType = API_VOUCHERKU_FISIK;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,sVoucherClass,page,nRecords, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"sVoucherClass",@"page",@"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_VOUCHERKU_FISIK andData:myString];
}
-(void)getVoucherKuDigital:(NSString *)memberId andVoucherClass:(NSString *)voucherClass andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords{
    
    mRequestType = API_VOUCHERKU_DIGITAL;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,voucherClass,sVoucherClass,page,nRecords, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"voucherClass",@"sVoucherClass",@"page",@"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_VOUCHERKU_DIGITAL andData:myString];
}

-(void)doLogout:(NSString *)deviceUniqueId andUsername:(NSString *)username andPassword:(NSString *)password{
    mRequestType = API_LOGOUT;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:deviceUniqueId,username,password, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"deviceUniqueId",@"username",@"password", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQuery:URL_LOGOUT andData:myString];
}

-(void)getSharingBalance:(NSString *)memberId andMerchantId:(NSString *)merchantId andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords{
    mRequestType = API_SHARING_BALANCE;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,merchantId, sVoucherClass, page,nRecords, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"merchantId",@"sVoucherClass",@"page", @"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQuery:URL_DEALS_SHARING_BALANCE andData:myString];
}

-(void)doDealsShare:(NSString *)memberIdFrom withMemberIdTo:(NSString *)memberIdTo withMerchantId:(NSString *)merchantId withDetails:(NSString *)details withVoucherId:(NSString *)voucherId withVoucherCode:(NSString *)voucherCode{
    mRequestType = API_DEAL_SHARE;
    
    //    NSArray *objects = [[NSArray alloc] initWithObjects:memberIdFrom,memberIdTo,merchantId,details, nil];
    
    NSDictionary *tempJSON3         = @{@"memberIdFrom": memberIdFrom,@"memberIdTo":memberIdTo,@"merchantId":merchantId,@"details": @[@{@"voucherId": voucherId,@"voucherCode": voucherCode}]};
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_DEALSHARE andData:myString];
    
}

-(void)doDealShare:(NSString *)memberIdFrom andMemberIdTo:(NSString *)memberIdTo andMerchantId:(NSString *)merchantId andDetails:(NSArray *)details{
    mRequestType = API_DEAL_SHARE;
    
    NSDictionary *tempJSON3         = @{@"memberIdFrom": memberIdFrom,@"memberIdTo":memberIdTo,@"merchantId":merchantId,@"details":details};
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_DEALSHARE andData:myString];
    
}

-(void)getDetailStore:(NSString *)productId andPage:(NSString *)page andNrecords:(NSString *)nRecords andKeyword:(NSString *)keyword andLowPrice:(NSString *)lowPriceFilter andHighPrice:(NSString *)highPriceFilter andIsDeals:(NSString *)isDeals andOrder:(NSString *)order andSortName:(NSString *)sortName andSortDate:(NSString *)sortCreatedData andSortPrice:(NSString *)sortPrice andVoucherClass:(NSString *)voucherClass{
    
    mRequestType                      = API_GETSTOREDETAIL;
    NSDictionary *productList         = @{@"id": productId,@"sVoucherClass":voucherClass,@"page": page, @"nRecords":nRecords,@"keyword": keyword,@"isDeals": isDeals,@"lowPriceFilter":lowPriceFilter,@"highPriceFilter":highPriceFilter,@"order": @{@"sortName": sortName,@"sortCreatedDate": sortCreatedData,@"sortPrice": sortPrice}};
    
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:productList options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQuery:URL_GETDETAILSTORE andData:myString];
    
}

-(void)getDetailStoreNew:(NSString *)productId andPage:(NSString *)page andNrecords:(NSString *)nRecords andKeyword:(NSString *)keyword andLowPrice:(NSString *)lowPriceFilter andHighPrice:(NSString *)highPriceFilter andIsDeals:(NSString *)isDeals andOrder:(NSString *)order andSortName:(NSString *)sortName andSortDate:(NSString *)sortCreatedData andSortPrice:(NSString *)sortPrice andVoucherClass:(NSString *)voucherClass andProductShownType:(NSString *)productShownType{
    
    mRequestType                      = API_GETSTOREDETAIL;
    NSDictionary *productList         = @{@"id": productId,@"sVoucherClass":voucherClass,@"page": page, @"nRecords":nRecords,@"keyword": keyword,@"isDeals": isDeals,@"lowPriceFilter":lowPriceFilter,@"highPriceFilter":highPriceFilter,@"productShownType":productShownType,@"order": @{@"sortName": sortName,@"sortCreatedDate": sortCreatedData,@"sortPrice": sortPrice}};
    
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:productList options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQuery:URL_GETDETAILSTORE andData:myString];
    
}

-(void)doCreateOrder:(NSString *)memberId withOrigin:(NSString *)origin withOrderNumber:(NSString *)orderNumber withOrders:(NSString *)orders withProductId:(NSString *)productId withVariantId:(NSString *)variantId withQuantity:(NSString *)quantity withMerchantId:(NSString *)merchantId withStoreId:(NSString *)storeId withDeliveryMethod:(NSString *)deliveryMethod withVoucherId:(NSString *)voucherId{
    
    mRequestType                    = API_ORDERCART;
    
    
    NSDictionary *tempJSON3         = @{@"memberId": memberId,@"origin": origin,@"orderNumber": orderNumber,@"orders": @[@{@"productId": productId,@"variantId": variantId,@"quantity": quantity,@"merchantId":merchantId,@"storeId": storeId,@"voucherId": voucherId,@"deliveryMethod":deliveryMethod}]};
    
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    [self doHttpPOSTQueryHeader:URL_CREATEORDER andData:myString];
    
}

-(void)getCartList:(NSString *)memberId andOrderNumber:(NSString *)orderNumber andStatus:(NSString *)status{
    
    mRequestType = API_CARTLIST;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,orderNumber,status, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"orderNumber",@"status", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_CARTLIST andData:myString];
}

-(void)getAddress:(NSString *)memberId{
    
    mRequestType = API_GETADDRESS;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_GETADDRESS andData:myString];
}

-(void)getPaymentList:(NSString *)memberId andPVoucherClass:(NSString *)voucherClass andOrderType:(NSString *)orderType{
    
    mRequestType = API_PAYMENT;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,voucherClass,orderType, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"purchasedVoucherClass",@"orderType", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_PAYMENT andData:myString];
    
}

-(void)getVA:(NSString *)memberId withOrderNumber:(NSString *)orderNumber withOrders:(NSString *)orders{
    
    mRequestType                    = API_VIRTUALACCOUNT;
    
    
    NSDictionary *tempJSON3         = @{@"memberId": memberId,@"orderNumber":orderNumber,@"orders": [SharedPreferences getListOrder]};
    
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_VA andData:myString];
}


-(void)getRewardList:(NSString *)keyword withOrder:(NSString *)order withPage:(NSString *)page withNrecords:(NSString *)nRecords withRewardCategory:(NSString *)rewardCategory withcityId:(NSString *)cityId withstoreId:(NSString *)storeId withmemberId:(NSString *)memberId{
    
    mRequestType                    = API_GETREWARD;
    
    
    NSDictionary *tempJSON3         = @{@"keyword": keyword,@"order":order,@"page":page,@"nRecords":nRecords,@"rewardCategory":rewardCategory,@"cityId":cityId,@"storeId":storeId,@"memberId":memberId};
    
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_GETREWARDLIST andData:myString];
    
    
}


-(void)getRewardById:(NSString *)rewardId{
    
    mRequestType = API_REWARDBYID;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:rewardId, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"rewardId", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_REWARDBYID andData:myString];
    
    
}

-(void)doRedeemReward:(NSString *)memberId withMerchantId:(NSString *)merchantId withStoreId:(NSString *)storeId withRewardId:(NSString *)rewardId withQuantity:(NSString *)quantity withPosTrans:(NSString *)posTransactionId withActor:(NSString *)actor{
    
    
    
    mRequestType = API_REDEEMREWARD;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,merchantId,storeId,rewardId,quantity,posTransactionId,actor, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"merchantId",@"storeId",@"rewardId",@"quantity",@"posTransactionId",@"actor", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_REDEEM andData:myString];
    
}

-(void)getRewardKu:(NSString *)keyword withOrder:(NSString *)order withPage:(NSString *)page withNrecords:(NSString *)nRecords withUserName:(NSString *)userName{
    
    
    mRequestType = API_REWARDKU;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:keyword,order,page,nRecords,userName, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"keyword",@"order",@"page",@"nRecords",@"username", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_REWARDKU andData:myString];
    
}

-(void)getHistoryVoucher:(NSString *)memberId withVoucherClass:(NSString *)sVoucherClass withPage:(NSString *)page withNrecords:(NSString *)nRecords{
    
    
    mRequestType = API_HISTORYVOUCHER;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,sVoucherClass,page,nRecords, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"sVoucherClass",@"page",@"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_HISTORYVOUCHER andData:myString];
    
}


-(void)getHistoryReward:(NSString *)username withnRecords:(NSString *)nRecords withPage:(NSString *)page withOrder:(NSString *)order withKeyword:(NSString *)keyword{
    
    mRequestType = API_HISTORYREWARD;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:username,nRecords,page,order,keyword, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"username",@"nRecords",@"page",@"order",@"keyword", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_HISTORYREWARD andData:myString];
    
}



-(void)doCheckMemberExist:(NSString *)listMember withUsername:(NSString *)username withMobileNumber:(NSString *)mobileNumber{
    
    mRequestType = API_CHECK_MEMBER_EXIST;
    
    NSDictionary *tempJSON3         = @{@"listMember": @[@{@"username": username,@"mobileNumber": mobileNumber}]};
    
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    [self doHttpPOSTQueryHeader:URL_CHECK_MEMBER_EXIST andData:myString];
    
    //    [self doHttpPOSTQuery:URL_CHECK_MEMBER_EXIST andData:myString];
    
}

-(void)doDiscardOrder:(NSString *)memberId withOrders:(NSString *)orders andOrderId:(NSString *)orderId withVariantId:(NSString *)variantId withQuantity:(NSString *)quantity withStatus:(NSString *)status withProductType:(NSString *)productType{
    
    mRequestType                    = API_DISCARDORDER;
    
    
    NSDictionary *tempJSON3         = @{@"memberId": memberId,@"orders": @[@{@"orderId": orderId,@"variantId": variantId,@"quantity": quantity,@"status":status,@"productType": productType}]};
    
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    [self doHttpPOSTQueryHeader:URL_DISCARDORDER andData:myString];
}

-(void)doUpdateProfile:(NSString *)memberID andFirstName:(NSString *)firstName andLastName:(NSString *)lastName andUserName:(NSString *)userName andBirthDate:(NSString *)birthDate andGender:(NSString *)gender andKTP:(NSString *)KTP andAllowNotification:(NSString *)allowNotification andBirthPlace:(NSString *)birthPlace andMotherName:(NSString *)mother andMobilePhone:(NSString *)phone{
    
    mRequestType = API_UBAHPROFILE;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberID,firstName,lastName,userName,birthDate,gender,KTP,allowNotification,birthPlace,mother,phone, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"firstName",@"lastName",@"userName",@"birthDate",@"gender",@"idNumber",@"allowNotification",@"birthPlace",@"motherName",@"mobilePhone", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_UBAHPROFILE andData:myString];
}

-(void)doResetPasswordMember:(NSString *)username andPassword:(NSString *)password andpasswordOld:(NSString *)passwordOld{
    
    mRequestType = API_UBAHPASSWORD;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:username,password,passwordOld, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"username",@"password",@"passwordOld", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_UBAHPASSWORD andData:myString];
}

-(void)doAddAddress:(NSString *)memberId andAddressAlias:(NSString *)addressAlias andRecipientName:(NSString *)recipientName andLine:(NSString *)line andStateProvId:(NSString*)stateProvId andCity:(NSString *)city andDistrict:(NSString *)district andVillage:(NSString *)village andPostalCode:(NSString *)postalCode andMobileNumber:(NSString *)mobileNumber andEmail:(NSString *)email{
    
    
    mRequestType =  API_ADDADRESS;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,addressAlias,recipientName,line,stateProvId,city,district,village,postalCode,mobileNumber,email, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"addressAlias",@"recipientName",@"line",@"stateProvId",@"city",@"district",@"village",@"postalCode",@"mobileNumber",@"email", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_ADDADRESS andData:myString];
    
}

//-(void)getOTP:(NSString *)memberId andPIN:(NSString *)pin{
//
//    mRequestType                    = API_OTPPASSWORD;
//
//    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,pin,nil];
//
//    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"pin", nil];
//
//    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
//
//    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
//    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
//
//    [self doHttpPOSTQueryHeader:URL_OTPPASSWORD andData:myString];
//}

-(void)getOTP:(NSString *)memberId andPIN:(NSString *)pin withOrderNumber:(NSString *)orderNumber{
    
    mRequestType                    = API_OTPPASSWORD;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,pin,orderNumber,nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"pin",@"orderNumber", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_OTPPASSWORD andData:myString];
}

-(void)doOTPPayment:(NSString *)memberId andPIN:(NSString *)pin andOTP:(NSString *)otp andTransactionId:(NSString *)transId andOrderNumber:(NSString *)orderNumber{
    
    mRequestType                    = API_OTPPAYCONFIRMED;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,pin,otp,transId,orderNumber,nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"pin",@"otp",@"transactionId",@"orderNumber", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_OTPCONFIRMED andData:myString];
}

-(void)doIssuePoint:(NSString *)memberId andmerchantId:(NSString *)merchantId andstoreId:(NSString *)storeId andposTransactionId:(NSString *)posTransactionId withissuedBy:(NSString *)issuedBy withpromotionTransactionDetail:(NSString *)promotionTransactionDetail withmemberTier:(NSString *)memberTier withReferralCode:(NSString *)referralCode withtotalTrxAmount:(NSString *)totalTrxAmount withpointAmount:(NSString *)pointAmount withvisitNumber:(NSString *)visitNumber withstampNumber:(NSString *)stampNumber withmillageTotal:(NSString *)millageTotal withsocialSharingNumber:(NSString *)socialSharingNumber{
    
    mRequestType                    = API_ISSUEPOINT;
    
    //    NSDictionary *tempJSON3         = @{@"memberId": memberId,@"merchantId": merchantId,@"storeId": storeId,@"posTransactionId": posTransactionId,@"issuedBy": issuedBy,@"promotionTransactionDetail": @[@{@"memberTier": memberTier,@"referralCode": referralCode,@"totalTrxAmount": totalTrxAmount,@"pointAmount":pointAmount,@"visitNumber": visitNumber,@"stampNumber": stampNumber,@"millageTotal":millageTotal,@"socialSharingNumber": socialSharingNumber}]};
    NSDictionary *tempJSON3         = @{@"memberId": memberId,@"merchantId": merchantId,@"storeId": storeId,@"posTransactionId": posTransactionId,@"issuedBy": issuedBy,@"promotionTransactionDetail": @{@"memberTier": memberTier,@"referralCode": referralCode,@"totalTrxAmount": totalTrxAmount,@"pointAmount":pointAmount,@"visitNumber": visitNumber,@"stampNumber": stampNumber,@"millageTotal":millageTotal,@"socialSharingNumber": socialSharingNumber}};
    
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    
    [self doHttpPOSTQueryHeader:URL_ISSUEPOINT andData:myString];
    
}

-(void)doIssuePointMed:(NSString *)memberId andmerchantId:(NSString *)merchantId andstoreId:(NSString *)storeId andposTransactionId:(NSString *)posTransactionId withissuedBy:(NSString *)issuedBy withpromotionTransactionDetail:(NSString *)promotionTransactionDetail withmemberTier:(NSString *)memberTier withReferralCode:(NSString *)referralCode withtotalTrxAmount:(NSString *)totalTrxAmount withpointAmount:(NSString *)pointAmount withvisitNumber:(NSString *)visitNumber withstampNumber:(NSString *)stampNumber withmillageTotal:(NSString *)millageTotal withsocialSharingNumber:(NSString *)socialSharingNumber{
    
    mRequestType                    = API_ISSUEPOINTMED;
    
    //    NSDictionary *tempJSON3         = @{@"memberId": memberId,@"merchantId": merchantId,@"storeId": storeId,@"posTransactionId": posTransactionId,@"issuedBy": issuedBy,@"promotionTransactionDetail": @[@{@"memberTier": memberTier,@"referralCode": referralCode,@"totalTrxAmount": totalTrxAmount,@"pointAmount":pointAmount,@"visitNumber": visitNumber,@"stampNumber": stampNumber,@"millageTotal":millageTotal,@"socialSharingNumber": socialSharingNumber}]};
    NSDictionary *tempJSON3         = @{@"memberId": memberId,@"merchantId": merchantId,@"storeId": storeId,@"posTransactionId": posTransactionId,@"issuedBy": issuedBy,@"promotionTransactionDetail": @{@"memberTier": memberTier,@"referralCode": referralCode,@"totalTrxAmount": totalTrxAmount,@"pointAmount":pointAmount,@"visitNumber": visitNumber,@"stampNumber": stampNumber,@"millageTotal":millageTotal,@"socialSharingNumber": socialSharingNumber}};
    
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    
    [self doHttpPOSTQueryHeader:URL_ISSUEPOINTMED andData:myString];
    
}

//-(void)doCCPayment:(NSString *)memberId andOrderNumber:(NSString *)orderNumber andChannel:(NSString *)channel andOrders:(NSString *)orders withProductId:(NSString *)productId withVariantId:(NSString *)variantId withQuantity:(NSString *)quantity withMerchantId:(NSString *)merchantId withorderId:(NSString *)orderId withtotalAmount:(NSString *)totalAmount withtaxAmount:(NSString *)taxAmount withdiscountValue:(NSString *)discountValue withshippingAmount:(NSString *)shippingAmount{
//
//    mRequestType                    = API_WEBVIEWCC;
//
//
//    NSDictionary *tempJSON3         = @{@"memberId": memberId,@"orderNumber": orderNumber,@"channel": channel,@"orders": @[@{@"productId": productId,@"variantId": variantId,@"quantity": quantity,@"merchantId":merchantId,@"orderId": orderId,@"totalAmount": totalAmount,@"discountValue":discountValue,@"shippingAmount":shippingAmount}]};
//
//
//    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
//    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
//
//    [self doHttpPOSTQueryHeader:URL_WEBVIEWCC andData:myString];
//
//}

-(void)doCCPayment:(NSString *)memberId andOrderNumber:(NSString *)orderNumber andReturnUrlChannel:(NSString *)returnUrlChannel andOrders:(NSString *)orders withorderId:(NSString *)orderId withtotalAmount:(NSString *)totalAmount withtaxAmount:(NSString *)taxAmount withdiscountValue:(NSString *)discountValue withshippingAmount:(NSString *)shippingAmount{
    
    mRequestType                    = API_WEBVIEWCC;
    
    
    NSDictionary *tempJSON3         = @{@"memberId": memberId,@"orderNumber": orderNumber,@"returnUrlChannel": returnUrlChannel,@"orders": @[@{@"orderId": orderId,@"totalAmount":totalAmount,@"discountValue":discountValue,@"shippingAmount":shippingAmount}]};
    
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_WEBVIEWCC andData:myString];
    
}


-(void)getListPayDigital:(NSString *)memberId andVoucherClass:(NSString *)voucherClass andsVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page withNRecords:(NSString *)nRecords{
    
    mRequestType                    = API_LISTPAYDIGITAL;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,voucherClass,sVoucherClass,page,nRecords,nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"voucherClass",@"sVoucherClass",@"page",@"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_LISTPAYDIGITAL andData:myString];
}

-(void)doPayDigital:(NSString *)memberId andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords{
    
    mRequestType = API_PAYDIGITAL;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,sVoucherClass,page,nRecords, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"sVoucherClass",@"page",@"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_VOUCHERKU_DIGITAL andData:myString];
    
}

-(void)getHistoryTopupWallet:(NSString *)memberId andDateFrom:(NSString *)dateFrom andDateTo:(NSString *)dateTo andPin:(NSString *)pin{
    
    mRequestType = API_HISTORYTOPUPWALLET;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,dateFrom,dateTo,pin, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"dateFrom",@"dateTo",@"pin", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_HISTORY_TOPUP_WALLET andData:myString];
}

-(void)getAmountSharing:(NSString *)memberId andMerchantId:(NSString *)merchantId andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords{
    mRequestType = API_AMOUNTSHARE;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,merchantId, sVoucherClass, page,nRecords, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"merchantId",@"sVoucherClass",@"page", @"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_AMOUNTSHARING andData:myString];
}

-(void)getAdvertisingSplashDetails:(NSString *)advertisingType andArticleCategory:(NSString *)articleCategory andCityId:(NSString *)cityId{
    mRequestType = API_ADVERTISING_DETAILS_SPLASH;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:advertisingType, articleCategory, cityId, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"advertisingType",@"articleCategory",@"cityId", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQuery:URL_ADVERTISING_DETAILS andData:myString];
}

-(void)doPayUltraVoucher:(NSString *)memberId andMerchantId:(NSString *)merchantId andOrderNumber:(NSString *)orderNumber andVoucherClass:(NSString *)voucherClass{
    
    
    mRequestType                    = API_PAYULTRAVOUCHER;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,merchantId,orderNumber,voucherClass,nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"merchantId",@"orderNumber",@"voucherClass", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_PAYDIGITAL andData:myString];
}

//-(void)getJneDestination:(NSString *)keyInput{
//
//    mRequestType = API_GET_JNE_DESTINATION;
//
//    NSArray *objects = [[NSArray alloc] initWithObjects:keyInput, nil];
//
//    NSArray *keys = [[NSArray alloc] initWithObjects:@"keyInput", nil];
//
//    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
//
//    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
//    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
//
//    [self doHttpPOSTQueryHeader:URL_GET_JNE_DESTINATION andData:myString];
//}

-(void)getJneDestination:(NSString *)keyInput andCity:(NSString *)city{
    
    mRequestType = API_GET_JNE_DESTINATION;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:keyInput,city, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"keyInput",@"city", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_GET_JNE_DESTINATION andData:myString];
}

-(void)checkJneRates:(NSString *)destinationCode{
    
    mRequestType = API_CHECK_JNE_RATES;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:destinationCode, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"destinationCode", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_CHECK_JNE_RATES andData:myString];
}

-(void)doSharingInquiry:(NSString *)memberId andMerchantId:(NSString *)merchantId andSVoucherClass:(NSString *)sVoucherClass andVoucherValue:(NSString *)voucherValue andQuantity:(NSString *)quantity andPage:(NSString *)page andNRecords:(NSString *)nRecords{
    
    mRequestType = API_SHARING_INQ;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,merchantId,sVoucherClass,voucherValue,quantity,page,nRecords, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"merchantId",@"sVoucherClass",@"voucherValue",@"quantity",@"page",@"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_SHARING_INQ andData:myString];
}

-(void)getRewardkuList:(NSString *)keyword andOrder:(NSString *)order andPage:(NSString *)page andNRecords:(NSString *)nRecords andUsername:(NSString *)username{
    
    mRequestType = API_REWARDKU_LIST;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:keyword,order,page,nRecords,username, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"keyword",@"order",@"page",@"nRecords",@"username", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_REWARDKU andData:myString];
}

-(void)doUpdateDeliveryAddress:(NSString *)memberId withOrders:(NSString *)orders withOrderId:(NSString *)orderId withDeliveryAddressId:(NSString *)deliveryAddressId withDeliveryMethod:(NSString *)deliveryMethod withShippingAmount:(NSString *)shippingAmount withShippingBy:(NSString *)shippingBy withShippingType:(NSString *)shippingType withPickupDate:(NSString *)pickupDate withPickupNote:(NSString *)pickupNote withStatus:(NSString *)status{
    
    mRequestType                    = API_UPDATE_DELIVERY_ADDRESS;
    
    
    NSDictionary *tempJSON3         = @{@"memberId": memberId,@"orders": @[@{@"orderId": orderId,@"deliveryAddressId": deliveryAddressId,@"deliveryMethod": deliveryMethod,@"shippingAmount":shippingAmount,@"shippingBy":shippingBy,@"shippingType":shippingType,@"pickupDate":pickupDate,@"pickupNote":pickupNote,@"status":status}]};
    
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    [self doHttpPOSTQueryHeader:URL_UPDATE_DELIVERY_ADDRESS andData:myString];
}

-(void)getUVBalanceVoucher:(NSString *)memberId andVoucherClass:(NSString *)voucherClass andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords{
    
    mRequestType = API_UVBALANCE_VOUCHER_LIST;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,voucherClass,sVoucherClass,page,nRecords, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"voucherClass",@"sVoucherClass",@"page",@"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_VOUCHERKU_DIGITAL andData:myString];
}

-(void)getStatusPembayaran:(NSString *)nRecords andorderStatus:(NSString *)orderStatus andPage:(NSString *)page andMemberId:(NSString *)memberId{
    
    mRequestType = API_STATUS_PEMBAYARAN;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:nRecords,orderStatus,page,memberId, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"nRecords",@"orderStatus",@"page",@"memberId", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_STATUS_PEMBAYARAN andData:myString];
}

-(void)getMemberPoint:(NSString*)memberId withOrder:(NSString*)order withPage:(NSString*)page withNrecords:(NSString*)nRecords{
    
    mRequestType = API_MEMBER_POINT;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,order,page,nRecords, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"order",@"page",@"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_CHECK_POINT_BALANCE andData:myString];
    
}

-(void)getProvinceList{
    mRequestType = API_PROVINCELIST;
    
    [self doHttpPOSTQuery:URL_PROVINCE_LIST andData:@""];
}

-(void)getCityList:(NSString *)stateProvId{
    mRequestType = API_CITYLIST;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:stateProvId, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"stateProvId", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQuery:URL_CITY_LIST andData:myString];
}

-(void)getDistrictList:(NSString *)cityId{
    mRequestType = API_DISTRICTLIST;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:cityId, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"cityId", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQuery:URL_DISTRICT_LIST andData:myString];
    
}

-(void)doRedeemVoucherDigital:(NSString*)memberId withVoucherId:(NSString*)voucherId withVoucherCode:(NSString*)voucherCode{
    mRequestType = API_REDEEM_VOUCHER_DIGITAL;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId, voucherId, voucherCode, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"voucherId",@"voucherCode", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_REDEEM_VOUCHER_DIGITAL andData:myString];
}
-(void)doRewardRedeem:(NSString*)memberId withMerchantId:(NSString*)merchantId withStoreId:(NSString*)storeId withRewardId:(NSString*)rewardId withQuantity:(NSString*)quantity withPosTransactionId:(NSString*)posTransactionId withActor:(NSString*)actor{
    
    mRequestType = API_REWARD_REDEEM;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId, merchantId, storeId, rewardId, quantity, posTransactionId,actor, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"merchantId",@"storeId","rewardId",@"quantity",@"posTransactionId",@"actor", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_REWARD_REDEEM andData:myString]; // URL_VOUCHER_REDEEM
}
-(void)doVoucherRedeem:(NSString*)memberId withRedemptionDate:(NSString*)redemptionDate withVoucherId:(NSString*)voucherId withVoucherCode:(NSString*)voucherCode withMerchantId:(NSString*)merchantId withStoreId:(NSString*)storeId withPin:(NSString*)pin withActor:(NSString*)actor{
    mRequestType = API_VOUCHER_REDEEM;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId, redemptionDate, voucherId,voucherCode,merchantId, storeId,pin,actor, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"redemptionDate",@"voucherId","voucherCode",@"merchantId",@"storeId",@"pin",@"actor", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_VOUCHER_REDEEM andData:myString];
}
-(void)doRewardRedeemNew:(NSString*)memberId withRedemptionDate:(NSString*)redemptionDate withVoucherId:(NSString*)voucherId withVoucherCode:(NSString*)voucherCode withMerchantId:(NSString*)merchantId withStoreId:(NSString*)storeId withPin:(NSString*)pin withActor:(NSString*)actor{
    
    mRequestType = API_REWARD_REDEEM_NEW;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId, redemptionDate, voucherId,voucherCode,merchantId, storeId,pin,actor, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId", @"redemptionDate", @"voucherId",@"voucherCode",@"merchantId",@"storeId",@"pin",@"actor", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_VOUCHER_REDEEM andData:myString];
    
}

-(void)getAmountFeeBillerProduct{
    mRequestType = API_BILLER_AMOUNT_FEE_PRODUCT;
    
    [self doHttpPOSTQueryHeader:URL_AMOUNT_FEE_BILLER_PRODUCT andData:@"{}"];
}

-(void)doBillerCheck:(NSString*)memberId withContractNo:(NSString*)contractNo withBillerId:(NSString*)billerId{
    mRequestType = API_BILLER_CHECK;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId, contractNo, billerId, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"contractNo",@"billerId", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_BILLER_CHECK andData:myString];
}

//-(void)doBillerInquiry:(NSString *)memberId withContractNo:(NSString *)contractNo withBillerId:(NSString *)billerId withPaymentMethod:(NSString *)paymentMethod withPin:(NSString *)pin{
//
//    mRequestType = API_BILLER_INQUIRY;
//
//    NSArray *objects = [[NSArray alloc] initWithObjects:memberId, contractNo, billerId,paymentMethod,pin, nil];
//
//    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"contractNo",@"billerId",@"paymentMethod",@"pin", nil];
//
//    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
//
//    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
//    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
//
//    [self doHttpPOSTQueryHeader:URL_BILLER_INQ andData:myString];
//}

-(void)doBillerInquiry:(NSString *)memberId withContractNo:(NSString *)contractNo withBillerId:(NSString *)billerId withPaymentMethod:(NSString *)paymentMethod withPin:(NSString *)pin withFavorite:(NSString *)favorite withOrigin:(NSString *)origin{
    
    mRequestType = API_BILLER_INQUIRY;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId, contractNo, billerId,paymentMethod,pin,favorite,origin, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"contractNo",@"billerId",@"paymentMethod",@"pin",@"favorite",@"origin", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_BILLER_INQ andData:myString];
}

-(void)doBillerPaymentWallet:(NSString *)memberId withContractNo:(NSString *)contractNo withBillerId:(NSString *)billerId withTrxId:(NSString *)trxId withPin:(NSString *)pin withOtp:(NSString *)otp{
    
    mRequestType = API_BILLER_PAYMENT_WALLET;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId, contractNo, billerId,trxId,pin,otp, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"contractNo",@"billerId",@"trxId",@"pin",@"otp", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_BILLER_PAYMENT_WALLET andData:myString];
}

-(void)getBillerFavorite:(NSString *)memberId withCategory:(NSString *)category{
    
    mRequestType = API_BILLER_FAVORITE;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId, category, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"category", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_BILLER_FAVORITE andData:myString];
}
-(void)doUpdateDeliveryAddress1:(NSString *)memberId withOrders:(NSArray *)orders{
    
    mRequestType                    = API_UPDATE_DELIVERY_ADDRESS1;
    
    
    NSDictionary *tempJSON3         = @{@"memberId": memberId,@"orders":orders};
    
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    [self doHttpPOSTQueryHeader:URL_UPDATE_DELIVERY_ADDRESS andData:myString];
}

-(void)getHistoryBillerVoucher:(NSString *)memberId andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords{
    
    mRequestType = API_HISTORY_BILLER_VOUCHER;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,sVoucherClass,page,nRecords, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"sVoucherClass",@"page",@"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_HISTORY_BILLER_VOUCHER andData:myString];
}

-(void)getHistoryBillerDetail:(NSString *)memberId andTransactionId:(NSString*)transactionId{
    
    mRequestType = API_HISTORY_BILLER_DETAIL;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,transactionId, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"transactionId", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_HISTORY_BILLER_DETAIL andData:myString];
    
}
-(void)getHistoryVoucherDetail:(NSString *)memberId andOrderNumber:(NSString *)orderNumber andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords{
    
    mRequestType = API_HISTORY_VOUCHER_DETAIL;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,orderNumber,sVoucherClass,page,nRecords, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"orderNumber",@"sVoucherClass",@"page",@"nRecords", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_HISTORY_VOUCHER_DETAIL andData:myString];
    
}

-(void)getPaymentUltraVoucherAmount:(NSString *)memberId andVoucherClass:(NSString *)voucherClass andStatus:(NSString *)status{
    mRequestType = API_PAYMENT_ULTRAVOUCHER_AMOUNT;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,voucherClass,status, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"voucherClass",@"status", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_PAYMENT_ULTRAVOUCHER_AMOUNT andData:myString];
}

-(void)doPaymentUltraVoucher:(NSString *)memberId withOrderNumber:(NSString *)orderNumber withVoucherClass:(NSString *)voucherClass WithVouchers:(NSArray *)vouchers{
    
    mRequestType                    = API_PAYMENTULTRAVOUCHER;
    
    
    NSDictionary *tempJSON3         = @{@"memberId": memberId,@"orderNumber": orderNumber,@"voucherClass": voucherClass,@"vouchers":vouchers};
    
    
    NSData *temp                    = [NSJSONSerialization dataWithJSONObject:tempJSON3 options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString              = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    [self doHttpPOSTQueryHeader:URL_PAYMENT_ULTRAVOUCHER andData:myString];
}


-(void)doRegisterFirebaseNotification:(NSString *)memberId withDeviceUniqueId:(NSString *)deviceUniqueId withRegKey:(NSString *)regKey{
    mRequestType = API_FIREBASE_NOTIFICATION;
    
    NSArray *objects = [[NSArray alloc] initWithObjects:memberId,deviceUniqueId,regKey, nil];
    
    NSArray *keys = [[NSArray alloc] initWithObjects:@"memberId",@"deviceUniqueId",@"regKey", nil];
    
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    NSData *temp = [NSJSONSerialization dataWithJSONObject:tempJsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *myString = [[NSString alloc] initWithData:temp encoding:NSUTF8StringEncoding];
    
    [self doHttpPOSTQueryHeader:URL_FIREBASE_NOTIFICATION andData:myString];
}
*/
@end

