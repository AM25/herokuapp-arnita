//
//  herokuApi.h
//  herokuApp-arnita
//
//  Created by Arnita Martiana on 17/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define API_GET_CONTACTS                         0
//#define API_SIGN_UP_VERIFICATION            1
//#define API_LOGIN                           2
//#define API_LOGOUT                          3
//#define API_FORGOT                          4
//#define API_RESET_PASSWORD                  5
//#define API_CHECK_WALLET_BALANCE            6
//#define API_UV_BALANCE                      7
//#define API_UPLOAD_MEMBER_PICTURE           8
//#define API_ADVERTISING_LIST                9
//#define API_ADVERTISING_DETAILS             10
//#define API_GETSTORELIST                    11
//#define API_SHARE_VOUCHERKU                 12
//#define API_UPLOAD_PHOTO                    13
//#define API_CHECK_MEMBER_EXIST              14
//#define API_AUTOREGISTER                    15
//#define API_VOUCHERKU_FISIK                 16
//#define API_VOUCHERKU_DIGITAL               17
//#define API_SHARING_BALANCE                 18
//#define API_DEAL_SHARE                      19
//#define API_GETSTOREDETAIL                  20
//#define API_ORDERCART                       21
//#define API_CARTLIST                        22
//#define API_GETADDRESS                      23
//#define API_PAYMENT                         24
//#define API_VIRTUALACCOUNT                  25
//#define API_GETREWARD                       26
//#define API_REWARDBYID                      27
//#define API_REDEEMREWARD                    28
//#define API_REWARDKU                        29
//#define API_HISTORYREWARD                   30
//#define API_HISTORYVOUCHER                  31
//#define API_DISCARDORDER                    32
//#define API_UBAHPROFILE                     33
//#define API_UBAHPASSWORD                    34
//#define API_ADDADRESS                       35
//#define API_HISTORYTOPUPWALLET              36
//#define API_OTPPASSWORD                     37
//#define API_OTPCONFIRMED                    38
//#define API_WEBVIEWCC                       39
//#define API_ISSUEPOINTMED                   40
//#define API_ISSUEPOINT                      41
//#define API_LISTPAYDIGITAL                  42
//#define API_PAYDIGITAL                      43
//#define API_AMOUNTSHARE                     44
//#define API_ADVERTISING_DETAILS_SPLASH      45
//#define API_PAYULTRAVOUCHER                 46
//#define API_GET_JNE_DESTINATION             47
//#define API_CHECK_JNE_RATES                 48
//#define API_SHARING_INQ                     49
//#define API_REWARDKU_LIST                   50
//#define API_UPDATE_DELIVERY_ADDRESS         51
//#define API_UVBALANCE_VOUCHER_LIST          52
//#define API_STATUS_PEMBAYARAN               53
//#define API_MEMBER_POINT                    54
//#define API_PROVINCELIST                    55
//#define API_CITYLIST                        56
//#define API_DISTRICTLIST                    57
//#define API_REDEEM_VOUCHER_DIGITAL          58
//#define API_REWARD_REDEEM                   59
//#define API_VOUCHER_REDEEM                  60
//#define API_REWARD_REDEEM_NEW               61
//#define API_BILLER_AMOUNT_FEE_PRODUCT       62
//#define API_BILLER_CHECK                    63
//#define API_BILLER_INQUIRY                  64
//#define API_BILLER_PAYMENT_WALLET           65
//#define API_OTPPAYCONFIRMED                 66
//#define API_BILLER_FAVORITE                 67
//#define API_UPDATE_DELIVERY_ADDRESS1        68
//#define API_HISTORY_BILLER_VOUCHER          69
//#define API_HISTORY_BILLER_DETAIL           70
//#define API_HISTORY_VOUCHER_DETAIL          71
//#define API_PAYMENT_ULTRAVOUCHER_AMOUNT     72
//#define API_PAYMENTULTRAVOUCHER             73
//#define API_FIREBASE_NOTIFICATION           74
//#define API_GETSTOREDETAILNEW               75

@protocol herokuAPIProtocol <NSObject>
-(void)onHerokuAPIDataReturned:(id)object withRequestedType:(int)type;
@end


@class AppDelegate;
@interface herokuAPI : NSObject <NSURLConnectionDataDelegate>{
@public
    
    id<herokuAPIProtocol> mProtocol;
    NSMutableData* responseData;
    NSURLConnection *urlConnection;
    NSURLSession *urlSession;
    int mRequestType;
    int mRespondCode;
    
    AppDelegate *mAppDelegate;
}
-(id)initWithData:(id<herokuAPIProtocol>)protocol;
//-(void)doSignUp:(NSString*) deviceUniqueId withUsername:(NSString*) username withPass:(NSString*)password withPhone:(NSString*)phone withFirstname:(NSString*)firstname withLastname:(NSString*)lastname withCopMember:(NSString*)copMember;
//-(void)doLogin:(NSString*)deviceUniqueId andUsername:(NSString*)username andPassword:(NSString*)password;
//-(void)doVerifikasi:(NSString*)username andCode:(NSString*)code;
//-(void)getWalletBalance:(NSString*)memberId;
//-(void)getPointBalance:(NSString*)memberId andPin:(NSString*)pin;
//-(void)getAdvertisingList;
//-(void)getAdvertisingDetails:(NSString*)advertisingType andArticleCategory:(NSString*)articleCategory andCityId:(NSString*)cityId;
//-(void)getUVBalance:(NSString*)memberId andVoucherClass:(NSString*)voucherClass andPage:(NSString*)page andNRecords:(NSString*)nRecords;
//-(void)GetStoreList:(NSString*)keyword withorder:(NSString*)order withpage:(NSString*)page withnRecords:(NSString*)nRecords withstoreType:(NSString*)storeType withmerchantId:(NSString*)merchantId withcityId:(NSString*)cityId withnearBy:(NSString*)nearBy withlatitude:(NSString*)latitude withLongitude:(NSString*)longitude withVoucherClass:(NSString*)sVoucherClass;
//-(void)getVoucherKu:(NSString *)memberId andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecorsd:(NSString *)nRecords;
//-(void)doUploadPhoto:(UIImage*)image;
//-(void)doAutoRegister:(NSString*)creator andUsername:(NSString*)username andFirstName:(NSString*)firstName andLastName:(NSString*)lastName andMobileNumber:(NSString*)mobileNumber;
//-(void)getForgotPasswordCode:(NSString *)email;
//-(void)doForgetPasswordWithUsername:(NSString *)username andVerificationCode:(NSString *)verificationCode andNewPassword:(NSString *)password;
//-(void)getVoucherKuFisik:(NSString *)memberId andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords;
//-(void)getVoucherKuDigital:(NSString *)memberId andVoucherClass:(NSString*)voucherClass andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords;
//-(void)doLogout:(NSString*)deviceUniqueId andUsername:(NSString*)username andPassword:(NSString*)password;
//-(void)getSharingBalance:(NSString *)memberId andMerchantId:(NSString*)merchantId andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords;
//-(void)doDealShare:(NSString *)memberIdFrom andMemberIdTo:(NSString*)memberIdTo andMerchantId:(NSString*)merchantId andDetails:(NSArray *)details;
//-(void)doDealsShare:(NSString*)memberIdFrom withMemberIdTo:(NSString*)memberIdTo withMerchantId:(NSString*)merchantId withDetails:(NSString*)details withVoucherId:(NSString*)voucherId withVoucherCode:(NSString*)voucherCode;
//-(void)getDetailStore:(NSString *)productId andPage:(NSString *)page andNrecords:(NSString *)nRecords andKeyword:(NSString *)keyword andLowPrice:(NSString *)lowPriceFilter andHighPrice:(NSString *)highPriceFilter andIsDeals:(NSString *)isDeals andOrder:(NSString *)order andSortName:(NSString *)sortName andSortDate:(NSString *)sortCreatedData andSortPrice:(NSString *)sortPrice andVoucherClass:(NSString *)voucherClass;
//-(void)getDetailStoreNew:(NSString *)productId andPage:(NSString *)page andNrecords:(NSString *)nRecords andKeyword:(NSString *)keyword andLowPrice:(NSString *)lowPriceFilter andHighPrice:(NSString *)highPriceFilter andIsDeals:(NSString *)isDeals andOrder:(NSString *)order andSortName:(NSString *)sortName andSortDate:(NSString *)sortCreatedData andSortPrice:(NSString *)sortPrice andVoucherClass:(NSString *)voucherClass andProductShownType:(NSString*)productShownType;
//-(void)doCreateOrder:(NSString*)memberId withOrigin:(NSString*)origin withOrderNumber:(NSString*)orderNumber withOrders:(NSString*)orders withProductId:(NSString*)productId withVariantId:(NSString*)variantId withQuantity:(NSString*)quantity withMerchantId:(NSString*)merchantId withStoreId:(NSString*)storeId withDeliveryMethod:(NSString*)deliveryMethod withVoucherId:(NSString*)voucherId;
//-(void)getCartList:(NSString *)memberId andOrderNumber:(NSString*)orderNumber andStatus:(NSString *)status;
//-(void)getAddress:(NSString *)memberId;
//-(void)getPaymentList:(NSString *)memberId andPVoucherClass:(NSString*)voucherClass andOrderType:(NSString*)orderType;
//-(void)getVA:(NSString*)memberId withOrderNumber:(NSString*)orderNumber withOrders:(NSArray*)orders;
//-(void)getRewardList:(NSString*)keyword withOrder:(NSString*)order withPage:(NSString*)page withNrecords:(NSString*)nRecords withRewardCategory:(NSString*)rewardCategory withcityId:(NSString*)cityId withstoreId:(NSString*)storeId withmemberId:(NSString*)memberId;
//-(void)getRewardById:(NSString *)rewardId;
//-(void)getRewardKu:(NSString*)keyword withOrder:(NSString*)order withPage:(NSString*)page withNrecords:(NSString*)nRecords withUserName:(NSString*)userName;
//-(void)doRedeemReward:(NSString*)memberId withMerchantId:(NSString*)merchantId withStoreId:(NSString*)storeId withRewardId:(NSString*)rewardId withQuantity:(NSString*)quantity withPosTrans:(NSString*)posTransactionId withActor:(NSString*)actor;
//-(void)getHistoryReward:(NSString*)username withnRecords:(NSString*)nRecords withPage:(NSString*)page withOrder:(NSString*)order withKeyword:(NSString*)keyword;
//-(void)getHistoryVoucher:(NSString*)memberId withVoucherClass:(NSString*)sVoucherClass withPage:(NSString*)page withNrecords:(NSString*)nRecords;
//-(void)doDiscardOrder:(NSString*)memberId withOrders:(NSString*)orders andOrderId:(NSString *)orderId withVariantId:(NSString*)variantId withQuantity:(NSString*)quantity withStatus:(NSString*)status withProductType:(NSString*)productType;
//-(void)doCheckMemberExist:(NSString*)listMember withUsername:(NSString*)username withMobileNumber:(NSString*)mobileNumber;
//-(void)doUpdateProfile:(NSString *)memberID andFirstName:(NSString *)firstName andLastName:(NSString *)lastName andUserName:(NSString *)userName andBirthDate:(NSString *)birthDate andGender:(NSString *)gender andKTP:(NSString *)KTP andAllowNotification:(NSString *)allowNotification andBirthPlace:(NSString *)birthPlace andMotherName:(NSString*)mother andMobilePhone:(NSString *)phone;
//-(void)doResetPasswordMember:(NSString *)username andPassword:(NSString *)password andpasswordOld:(NSString *)passwordOld;
//-(void)doAddAddress:(NSString*)memberId andAddressAlias:(NSString *)addressAlias andRecipientName:(NSString*)recipientName andLine:(NSString *)line andStateProvId:(NSString*)stateProvId andCity:(NSString *)city andDistrict:(NSString*)district andVillage:(NSString *)village andPostalCode:(NSString *)postalCode andMobileNumber:(NSString*)mobileNumber andEmail:(NSString *)email;
//-(void)getHistoryTopupWallet:(NSString *)memberId andDateFrom:(NSString*)dateFrom andDateTo:(NSString *)dateTo andPin:(NSString*)pin;
////-(void)getOTP:(NSString *)memberId andPIN:(NSString *)pin;
//-(void)getOTP:(NSString *)memberId andPIN:(NSString *)pin withOrderNumber:(NSString*)orderNumber;
//-(void)doOTPPayment:(NSString *)memberId andPIN:(NSString *)pin andOTP:(NSString *)otp andTransactionId:(NSString *)transId andOrderNumber:(NSString*)orderNumber;
////-(void)doCCPayment:(NSString *)memberId andOrderNumber:(NSString *)orderNumber andChannel:(NSString *)channel andOrders:(NSString *)orders withProductId:(NSString*)productId withVariantId:(NSString*)variantId withQuantity:(NSString*)quantity withMerchantId:(NSString*)merchantId withorderId:(NSString*)orderId withtotalAmount:(NSString*)totalAmount withtaxAmount:(NSString*)taxAmount withdiscountValue:(NSString*)discountValue withshippingAmount:(NSString*)shippingAmount;
//-(void)doCCPayment:(NSString *)memberId andOrderNumber:(NSString *)orderNumber andReturnUrlChannel:(NSString *)returnUrlChannel andOrders:(NSString *)orders withorderId:(NSString*)orderId withtotalAmount:(NSString*)totalAmount withtaxAmount:(NSString*)taxAmount withdiscountValue:(NSString*)discountValue withshippingAmount:(NSString*)shippingAmount;
//-(void)doIssuePoint:(NSString *)memberId andmerchantId:(NSString *)merchantId andstoreId:(NSString *)storeId andposTransactionId:(NSString *)posTransactionId withissuedBy:(NSString*)issuedBy withpromotionTransactionDetail:(NSString*)promotionTransactionDetail withmemberTier:(NSString*)memberTier withReferralCode:(NSString*)referralCode withtotalTrxAmount:(NSString*)totalTrxAmount withpointAmount:(NSString*)pointAmount withvisitNumber:(NSString*)visitNumber withstampNumber:(NSString*)stampNumber withmillageTotal:(NSString*)millageTotal withsocialSharingNumber:(NSString*)socialSharingNumber;
//-(void)doIssuePointMed:(NSString *)memberId andmerchantId:(NSString *)merchantId andstoreId:(NSString *)storeId andposTransactionId:(NSString *)posTransactionId withissuedBy:(NSString*)issuedBy withpromotionTransactionDetail:(NSString*)promotionTransactionDetail withmemberTier:(NSString*)memberTier withReferralCode:(NSString*)referralCode withtotalTrxAmount:(NSString*)totalTrxAmount withpointAmount:(NSString*)pointAmount withvisitNumber:(NSString*)visitNumber withstampNumber:(NSString*)stampNumber withmillageTotal:(NSString*)millageTotal withsocialSharingNumber:(NSString*)socialSharingNumber;
//-(void)getListPayDigital:(NSString *)memberId andVoucherClass:(NSString *)voucherClass andsVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page withNRecords:(NSString *)nRecords;
//-(void)doPayDigital:(NSString *)memberId andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords;
//-(void)getAmountSharing:(NSString *)memberId andMerchantId:(NSString*)merchantId andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords;
//-(void)getAdvertisingSplashDetails:(NSString*)advertisingType andArticleCategory:(NSString*)articleCategory andCityId:(NSString*)cityId;
//-(void)doPayUltraVoucher:(NSString *)memberId andMerchantId:(NSString *)merchantId andOrderNumber:(NSString *)orderNumber andVoucherClass:(NSString *)voucherClass;
////-(void)getJneDestination:(NSString *)keyInput;
//-(void)getJneDestination:(NSString *)keyInput andCity:(NSString*)city;
//-(void)checkJneRates:(NSString *)destinationCode;
//-(void)doSharingInquiry:(NSString *)memberId andMerchantId:(NSString*)merchantId andSVoucherClass:(NSString *)sVoucherClass andVoucherValue:(NSString*)voucherValue andQuantity:(NSString*)quantity andPage:(NSString *)page andNRecords:(NSString *)nRecords;
//-(void)getRewardkuList:(NSString*)keyword andOrder:(NSString*)order andPage:(NSString*)page andNRecords:(NSString*)nRecords andUsername:(NSString*)username;
//-(void)doUpdateDeliveryAddress:(NSString*)memberId withOrders:(NSString*)orders withOrderId:(NSString*)orderId withDeliveryAddressId:(NSString*)deliveryAddressId withDeliveryMethod:(NSString*)deliveryMethod withShippingAmount:(NSString*)shippingAmount withShippingBy:(NSString*)shippingBy withShippingType:(NSString*)shippingType withPickupDate:(NSString*)pickupDate withPickupNote:(NSString*)pickupNote withStatus:(NSString*)status;
//-(void)getUVBalanceVoucher:(NSString *)memberId andVoucherClass:(NSString*)voucherClass andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords;
//-(void)getStatusPembayaran:(NSString*)nRecords andorderStatus:(NSString*)orderStatus andPage:(NSString*)page andMemberId:(NSString*)memberId;
//-(void)getMemberPoint:(NSString*)memberId withOrder:(NSString*)order withPage:(NSString*)page withNrecords:(NSString*)nRecords;
//-(void)getProvinceList;
//-(void)getCityList:(NSString*)stateProvId;
//-(void)getDistrictList:(NSString*)cityId;
//-(void)doRedeemVoucherDigital:(NSString*)memberId withVoucherId:(NSString*)voucherId withVoucherCode:(NSString*)voucherCode;
//-(void)doRewardRedeem:(NSString*)memberId withMerchantId:(NSString*)merchantId withStoreId:(NSString*)storeId withRewardId:(NSString*)rewardId withQuantity:(NSString*)quantity withPosTransactionId:(NSString*)posTransactionId withActor:(NSString*)actor;
//-(void)doVoucherRedeem:(NSString*)memberId withRedemptionDate:(NSString*)redemptionDate withVoucherId:(NSString*)voucherId withVoucherCode:(NSString*)voucherCode withMerchantId:(NSString*)merchantId withStoreId:(NSString*)storeId withPin:(NSString*)pin withActor:(NSString*)actor;
//-(void)doRewardRedeemNew:(NSString*)memberId withRedemptionDate:(NSString*)redemptionDate withVoucherId:(NSString*)voucherId withVoucherCode:(NSString*)voucherCode withMerchantId:(NSString*)merchantId withStoreId:(NSString*)storeId withPin:(NSString*)pin withActor:(NSString*)actor;
//-(void)getAmountFeeBillerProduct;
//-(void)doBillerCheck:(NSString*)memberId withContractNo:(NSString*)contractNo withBillerId:(NSString*)billerId;
////-(void)doBillerInquiry:(NSString*)memberId withContractNo:(NSString*)contractNo withBillerId:(NSString*)billerId withPaymentMethod:(NSString*)paymentMethod withPin:(NSString*)pin;
//-(void)doBillerInquiry:(NSString*)memberId withContractNo:(NSString*)contractNo withBillerId:(NSString*)billerId withPaymentMethod:(NSString*)paymentMethod withPin:(NSString*)pin withFavorite:(NSString*)favorite withOrigin:(NSString*)origin;
//-(void)doBillerPaymentWallet:(NSString*)memberId withContractNo:(NSString*)contractNo withBillerId:(NSString*)billerId withTrxId:(NSString*)trxId withPin:(NSString*)pin withOtp:(NSString*)otp;
//-(void)getBillerFavorite:(NSString*)memberId withCategory:(NSString*)category;
//-(void)doUpdateDeliveryAddress1:(NSString*)memberId withOrders:(NSArray*)orders;
//-(void)getHistoryBillerVoucher:(NSString *)memberId andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords;
//-(void)getHistoryBillerDetail:(NSString *)memberId andTransactionId:(NSString*)transactionId;
//-(void)getHistoryVoucherDetail:(NSString *)memberId andOrderNumber:(NSString*)orderNumber andSVoucherClass:(NSString *)sVoucherClass andPage:(NSString *)page andNRecords:(NSString *)nRecords;
//-(void)getPaymentUltraVoucherAmount:(NSString *)memberId andVoucherClass:(NSString *)voucherClass andStatus:(NSString *)status;
//-(void)doPaymentUltraVoucher:(NSString*)memberId withOrderNumber:(NSString*)orderNumber withVoucherClass:(NSString*)voucherClass WithVouchers:(NSArray*)vouchers;
//-(void)doRegisterFirebaseNotification:(NSString*)memberId withDeviceUniqueId:(NSString*)deviceUniqueId withRegKey:(NSString*)regKey;

@end
