//
//  ViewController.m
//  herokuApp-arnita
//
//  Created by Arnita Martiana on 17/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import "ViewController.h"
#import "ContactDetailsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ViewController ()

@end

@implementation ViewController
@synthesize tableViewContactData;
@synthesize searchbarContacts;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"viewDidLoad");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://simple-contact-crud.herokuapp.com/contact"]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        //        NSLog(@"Request reply: %@", requestReply);
        
        NSDictionary *dict = [self cleanJsonToObject:data];
        
        mDatalist = [dict valueForKey:@"data"];
        NSDictionary *dict2 = [dict valueForKey:@"data"];
        NSLog(@"arrayTableData1:%@", mDatalist);
        //        NSLog(@"dict2:%@", dict2);
    }] resume];
    
    [self getRequest];

    
    mDatalist=[[NSUserDefaults standardUserDefaults] objectForKey:@"arrayContacts"];
    NSLog(@"mdata:%@", mDatalist);

    mRefreshControl = [[UIRefreshControl alloc] init];
    
    // Configure Refresh Control
    [mRefreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
    
    [self.tableViewContactData addSubview:mRefreshControl];
    
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"+" style:UIBarButtonItemStylePlain target:self action:@selector(addAction:)];
    
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor grayColor];
    
    self.tableViewContactData.delegate=self;
    self.tableViewContactData.dataSource=self;
    
    self.tableViewContactData.allowsMultipleSelectionDuringEditing = NO;
}

-(void)addAction:(UIBarButtonItem *)sender{
    
    [self performSegueWithIdentifier:@"addContact" sender:self];
    
}

-(void)refreshData:(NSObject*)sender{
    NSLog(@"refresh");
    if(sender==nil){
        //        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://simple-contact-crud.herokuapp.com/contact"]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        //        NSLog(@"Request reply: %@", requestReply);
        
        NSDictionary *dict = [self cleanJsonToObject:data];
        
        mDatalist = [dict valueForKey:@"data"];
        NSDictionary *dict2 = [dict valueForKey:@"data"];
        NSLog(@"arrayTableDataRefresh:%@", mDatalist);
        //        NSLog(@"dict2:%@", dict2);
    }] resume];
    
    [self getRequest];
    
    
    [tableViewContactData reloadData];
 
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self refreshData:nil];

    
    NSLog(@"viewDidLayoutSubviews");
    [self.tableViewContactData reloadData];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self refreshData:nil];
    NSLog(@"viewWillAppear");
    //    [self refreshData:nil];
    [self.tableViewContactData reloadData];
}

- (void)main
{
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.tableViewContactData reloadData];
    });
}

-(void)getRequest{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://simple-contact-crud.herokuapp.com/contact"]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        //        NSLog(@"Request reply: %@", requestReply);
        
        NSDictionary *dict = [self cleanJsonToObject:data];
        
        mDatalist = [dict valueForKey:@"data"];
        NSDictionary *dict2 = [dict valueForKey:@"data"];
        NSLog(@"arrayTableData2:%@", mDatalist);
//        NSLog(@"dict2:%@", dict2);
    }] resume];
    
    [self.tableViewContactData reloadData];
}

-(void)loadJSON {
    
    NSURL * url = [NSURL URLWithString:@"https://simple-contact-crud.herokuapp.com/contact"];
    NSURLSession *session=[NSURLSession sharedSession];
    
    [session dataTaskWithRequest:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error!=nil) {
            NSLog(@"Something went wrong:%@",error);
        } else {
            NSError *jsonError;
            NSDictionary * jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
            if (jsonError != nil) {
                NSLog(@"JSON isn't right:%@",jsonError);
            } else {
                mDatalist = jsonDict[@"data"];
                dispatch_async(dispatch_get_main_queue(),^{
                    [self.tableViewContactData reloadData];
                });
            }
        }
        
    }];
    
    [self.tableViewContactData reloadData];
}


- (id)cleanJsonToObject:(id)data
{
    NSError* error;
    if (data == (id)[NSNull null])
    {
        return [[NSObject alloc] init];
    }
    id jsonObject;
    if ([data isKindOfClass:[NSData class]])
    {
        jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    } else
    {
        jsonObject = data;
    }
    if ([jsonObject isKindOfClass:[NSArray class]])
    {
        NSMutableArray *array = [jsonObject mutableCopy];
        for (int i = (int)array.count-1; i >= 0; i--)
        {
            id a = array[i];
            if (a == (id)[NSNull null])
            {
                [array removeObjectAtIndex:i];
            } else
            {
                array[i] = [self cleanJsonToObject:a];
            }
        }
        return array;
    } else if ([jsonObject isKindOfClass:[NSDictionary class]])
    {
        NSMutableDictionary *dictionary = [jsonObject mutableCopy];
        for(NSString *key in [dictionary allKeys])
        {
            id d = dictionary[key];
            if (d == (id)[NSNull null])
            {
                dictionary[key] = @"";
            } else
            {
                dictionary[key] = [self cleanJsonToObject:d];
            }
        }
        return dictionary;
    } else
    {
        return jsonObject;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"details"]) {
        
        ContactDetailsViewController *detailsViewController = segue.destinationViewController;
        detailsViewController.contactDetails =contactDetails;
    }
}

#pragma mark - UITableView Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"count array: %i", mDatalist.count);
    return  mDatalist.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static  NSString *strCell = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strCell];
    if(cell==nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strCell];
    }
    NSString *nama=[[mDatalist valueForKey:@"firstName"]objectAtIndex:indexPath.row];
    NSLog(@"nama:%@", nama);
    cell.textLabel.text = nama;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    contactDetails = [mDatalist objectAtIndex:indexPath.row];
    NSLog(@"contactDetails:%@", contactDetails);
    
    [self performSegueWithIdentifier:@"details" sender:self];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *idStr=[[mDatalist valueForKey:@"id"] objectAtIndex:indexPath.row];
    NSLog(@"conDet:%@", idStr);
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSLog(@"klik delete");
        
        NSString *urlString = [NSString stringWithFormat:@"https://simple-contact-crud.herokuapp.com/contact/%@",idStr];
        NSLog(@"url delete:%@", urlString);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setHTTPMethod:@"DELETE"];
        

        
        NSError *err;
        NSURLResponse *response;
        
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
        
        NSString *resSrt = [[NSString alloc]initWithData:responseData encoding:NSASCIIStringEncoding];
        NSLog(@"res delete:%@", resSrt);
        
        [self refreshData:nil];
        
       
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"Did Scroll");
    [tableViewContactData reloadData];
    
}
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
