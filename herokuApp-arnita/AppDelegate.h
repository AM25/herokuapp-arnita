//
//  AppDelegate.h
//  herokuApp-arnita
//
//  Created by Arnita Martiana on 17/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

