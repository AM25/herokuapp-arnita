//
//  ContactDetailsViewController.h
//  herokuApp-arnita
//
//  Created by Arnita Martiana on 18/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFloatLabelTextField.h"

@interface ContactDetailsViewController : UIViewController{
    NSDictionary *dict;
    NSString *message;
}
@property (strong, nonatomic) NSArray *contactDetails;


@property (weak, nonatomic) IBOutlet UIImageView *imgProfil;
@property (weak, nonatomic) IBOutlet UILabel *lbFullName;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *lbFirstName;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *lbLastName;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *lbAge;
- (IBAction)btnSave:(id)sender;

@end
